<?php

namespace App\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use Sentinel;
use Str;
use DB;
use Mail;
use Session;
use Stripe;
use Illuminate\Support\Facades\Validator;

class VerifyProfileController extends Controller
{
    public function __construct()
    {
        add_action('hh_dashboard_breadcrumb', [$this, '_addCreatePageButton']);
    }

   public function index()
   {
       
      $profiledoc = DB::table('profile_verification_docs')->where(array('user_id'=>get_current_user_id()))->first();
    $message ='';
    if(!empty($profiledoc) and $profiledoc->is_verified)
       {
           $message = $profiledoc->admin_comment;
           
       }
       if(!empty($profiledoc) and !$profiledoc->is_verified)
       {
           $message = 'You document verification  is under progress.';
       }
       
    return view('frontend.document.verify',compact('message','profiledoc'));
    
   }

    /****** new function for verfication view for stripe **********/ 
    public function verifystripe()
    {
      $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
      $user_id = get_current_user_id();
      $secret_key = env('STRIPE_SECRET');
      $publish_key = env('STRIPE_KEY');
      //dd($user_id);
      if($user_id)
      {
        //dd($user_id);
     
       

// In the route handler for /create-verification-session:
// Authenticate your user

// Create the session
$verification_session = $stripe->identity->verificationSessions->create([
  'type' => 'document',
  'metadata' => [
    'user_id' => '{{$user_id}}',
  ],
]);
//dd($verification_session);

$data= json_encode(['client_secret' => $verification_session->client_secret, 'id' => $verification_session->id]);


//now checking response from api for user id

//$verification_session_user = $stripe->identity->verificationSessions->retrieve(
  //$user_id,
  //[]
//);
//$verification_session = $stripe->identity->verificationSessions->all([
 // 'limit' => 0,
//]);
//echo "<pre>"; print_r($verification_session_user);
//echo json_encode(['client_secret' => $verification_session_user->client_secret, 'id' => $verification_session_user->id]);


//die('check json value');
//dd($verification_session );

// Return only the client secret to the frontend.
//$client_secret = $verification_session->client_secret;
//dd($client_secret);

      }
      else
      {

      }
      //dd('just testing the function');
      return view('frontend.document.verification',compact('user_id','data','secret_key','publish_key'));
    }

    /****** verify function ends here **********/
    /****** new functin check status from user id **********/
    public function checkverifystatus($id)
    {
      //dd($id);
      $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
      //$id = $request->id;

   $verification_session = $stripe->identity->verificationSessions->retrieve(
   $id,
  []
);
  // dd($verification_session);
//$verification_session = $stripe->identity->verificationSessions->all([
 // 'limit' => 0,
//]);
  //echo "<pre>"; print_r($verification_session);
 $data=  json_encode(['client_secret' => $verification_session->client_secret, 'id' => $verification_session->id]);
// dd($data);
//die("stop here for testings");
  $current_status =$this->verifyall();
  //dd($current_status);
  //die('stop here for status check');
  return view('frontend.document.submitted',compact('current_status'));

    }

    /****new function for all.php code****/

    public function verifyall()
    {
      $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
      $verification_session = $stripe->identity->verificationSessions->all([
     'limit' => 1,
      ]);
     //echo "<pre>"; print_r($verification_session);
       $status= $verification_session->data[0]->status;
       return $status;
    

    }

  
  /****** save Verification document **********/ 
   public function saveVerificationDoc(Request $request)
   {
       
       try
       {
           
       
          $validated = Validator::make($request->all(),[
              'document_type'=>'required',
              'document_no'=>'required',
        'front_file' => 'required',
        'back_file' => 'required',
          ], [
    'front_file.required' => 'The Clear photo of ID  is required.',
    'back_file.required' => 'The Clear Selfy with ID  is required.',  
]);
             
             if ($validated->fails())
                {
                     return redirect()->back()->withErrors($validated)->withInput();
                }  
           
        // $front_file =  $request->file('front_file');
         if($files=$request->file('front_file')){  
            // Get filename with the extension
            $filenameWithExt = $request->file('front_file')->getClientOriginalName();
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('front_file')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = time().'front.'.$extension;
            // Upload Image
            $path = $request->file('front_file')->storeAs('public/documents',$fileNameToStore);

          $front_file = $fileNameToStore;
            }
         
        if($files=$request->file('back_file')){  
            // Get filename with the extension
            $filenameWithExt = $request->file('back_file')->getClientOriginalName();
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('back_file')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = time().'back.'.$extension;
            // Upload Image
            $path = $request->file('back_file')->storeAs('public/documents',$fileNameToStore);
           $back_file =  $fileNameToStore;
          
            }
         $alldata = $request->all();
         $alldata['front_file']  = $front_file;
         $alldata['back_file']   = $back_file;
         $alldata['is_verified']   = 0;
         $alldata['user_id']   = get_current_user_id();
          unset($alldata['_token']);
         
         if(DB::table('profile_verification_docs')->insert($alldata))
         {
             
         
        $admin_data = get_admin_user();
        $admin_email = $admin_data->email;
        $from = get_option('email_from_address');
        $from_name = get_option('email_from');
        $data =    $profiledoc = DB::table('profile_verification_docs')->where(array('user_id'=>get_current_user_id()))->first();
        Mail::send('mail', array('maildata'=>$data), function($message)use($admin_data,$from,$from_name) {
         $message->to( $admin_data->email, $admin_data->first_name)->subject
            ( 'Profile Document approval request');
         $message->from(get_option('email_from_address', ''),get_option('email_from', ''));
      });
       /******* Send user email for document verification */
       $user = get_user_by_id(get_current_user_id());
        Mail::send('docthanks', array('maildata'=>$data), function($message)use($user) {
         $message->to( $user->email, $user->first_name)->subject   
            ( 'Your Soundbox Studios ID Verification Underway');
         $message->from(get_option('email_from_address', ''),get_option('email_from', ''));  
      });
        
       
      
   
        
              \Session::flash('success','Thanks for the document submission.We will get back to you within 24 hours.'); 
         }
         else
         {
              \Session::flash('error','some error occured'); 
             
         }
         
       }catch(Exception $ex)
       {
           \Session::flash('error',$ex->getMessage()); 
       }
        return redirect()->back();
         
               
                        
       
       
   }

}
