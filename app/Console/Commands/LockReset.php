<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class LockReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:lockreset';


    private $ksaEmail ='hello@soundboxstudios.com.au';
    private $ksaPassword='Currie131!';
    private $ksaLoginUrl ='https://cloud.kas.com.au/api/users/apikey';
    private $lockPinCreateUrl ='https://cloud.kas.com.au/api/lockCommand/remotePassword';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lock reset on Ksa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->get_login_auth_key();
          $this->setLockPassword('SS010033',1182,'test','1629770400','1629774000','test@cyberframe.in');
         die();
      // $this->setLockPassword('SS010033',1025,'test',time(),strtotime('+2 hours'),'testing@cyberframe.in');
        $bookings = \DB::table('booking')->where('start_time','>=',time())->where('end_time','<=',time())->where('status','=','completed')->get();
        foreach($bookings as $booking):
          if(empty(get_user_meta($booking->buyer,'keysent_'.$booking->ID,'')))
          {

              $this->setLockPassword('SS010033',get_user_meta($booking->buyer,'keysent_'.$booking->ID,''),$booking->first_name.''.$booking->last_name,$booking->start_time,$booking->end_time,$booking->email);
                  
             update_user_meta($booking->buyer,'keysent_'.$booking->ID,true);
          }
        endforeach;
        return 0;
    }

    /**
 * get api key before change lock pin
 */
    public function get_login_auth_key()
    {
        
            Log::info('KSA logging',[$this->differenceInHours(get_user_meta(1002,'ksaApiKeyDateTime',''),time()),get_user_meta(1002,'ksaApiKeyDateTime',''),time()]);  
                  Log::info('KSA logging');  
     
       if(empty(get_user_meta(1002,'ksaApiKeyDateTime','')) || get_user_meta(1002,'ksaApiKeyDateTime','')<time()) 
       {
            Log::info('Logged in user with params',[
                    'email' => $this->ksaEmail,
                    'password' => $this->ksaPassword,
                ]);
                $response = Http::post($this->ksaLoginUrl, [
                    'email' => $this->ksaEmail,
                    'password' => $this->ksaPassword,
                ]);

            // dd($response->body());

            Log::info('KSA Logged in user reponse',['response'=>$response->body()]);  
            update_user_meta(1001,'ksaApiKey',$response->body());
            update_user_meta(1002,'ksaApiKeyDateTime',strtotime('+22 hours'));
     }
    }


    /********
     * Set Lock pin based on room type
     */
    public function setLockPassword($factorynumber,$pin,$name,$passwordstarttime,$passwordendtime,$email)
    {
        $this->get_login_auth_key();  
            $starttime = explode(':',date('G:i',$passwordstarttime));
         $endtime   = explode(':',date('G:i',$passwordendtime));
       // date_default_timezone_set('Australia/Queensland');
        Log::info('Logged in user with params',[
            "password_id"=>rand(101,200),
              "password_start_time"=>date('Ymd',$passwordstarttime).($starttime[0]<10?'0'.$starttime[0]:$starttime[0]).'00',
            "password_end_time"=>date('Ymd',$passwordendtime).($endtime[0]<10?'0'.$endtime[0]:$endtime[0]).'00', 
            "password"=>$pin,
            "password_name"=>$name,
            "email_checked"=> false,
            "password_email"=>$email,
            "password_message"=>"Welcome to our studio".date('G:i',$passwordstarttime),
         ]);
         
         
     
         $params = [
            "password_id"=>rand(101,200),
            "password_start_time"=>date('Ymd',$passwordstarttime).($starttime[0]<10?'0'.$starttime[0]:$starttime[0]).'00',
            "password_end_time"=>date('Ymd',$passwordendtime).($endtime[0]<10?'0'.$endtime[0]:$endtime[0]).'00', 
            "password"=>strval($pin),
            "password_name"=>$name,
            "email_checked"=> false,    
            "password_email"=>$email,
            "password_message"=>"Welcome to our studio"
         ];
          //dd($params);
        $response = Http::withHeaders([
            'x-auth-token' => get_user_meta('1001','ksaApiKey','')
        ])->post($this->lockPinCreateUrl.'/'.$factorynumber,$params);
     /* dd([
        "password_id"=>rand(101,200),
        "password_start_time"=>'"'.date('Ymd',$passwordstarttime).str_replace(':','',date('h:i',$passwordstarttime)).'"',
        "password_end_time"=>'"'.date('Ymd',$passwordendtime).str_replace(':','',date('h:i',$passwordendtime)).'"',  
        "password"=>"'".$pin."'",
        "password_name"=>$name,
        "email_checked"=> false,    
        "password_email"=>$email,
        "password_message"=>"Welcome to our studio"
     ]); */
      
       //dd($response->body());

       Log::info('Reset Pin Logged reponse',['response'=>$response->body()]);
     

    }
   


    function differenceInHours($startdate,$enddate){
        $starttimestamp = strtotime($startdate);
        $endtimestamp = strtotime($enddate);
        $difference = abs($endtimestamp - $starttimestamp)/3600;
        return $difference;
    }
}
