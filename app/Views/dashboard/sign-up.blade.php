@include('dashboard.components.header')

<style>
.iti {

    width: 100%;
}
</style>
<div class="account-pages hh-dashboard mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card bg-pattern">
                    <div class="card-body p-4">

                        <div class="text-center w-75 m-auto">
                            <a class="logo" href="{{ dashboard_url() }}">
                                <?php
                                $logo = get_option('dashboard_logo');
                                $logo_url = get_attachment_url($logo);
                                ?>
                                <img src="{{ $logo_url }}" alt="{{get_attachment_alt($logo)}}">
                            </a>
                            <p class="text-muted mb-4 mt-3">{{__("Don't have an account? Create your account, it takes less than a minute")}}</p>
                        </div>

                        <form id="hh-sign-up-form" action="{{ url('auth/sign-up') }}" method="post"
                              data-reload-time="1500"
                              data-validation-id="form-sign-up"
                              class="form form-action">
                            @include('common.loading')
                            <div class="form-group">
                                <label for="first-name">{{__('First Name')}}</label>
                                <input class="form-control" type="text" id="first-name" name="first_name"
                                       placeholder="{{__('First Name')}}">
                            </div>
                            <div class="form-group">
                                <label for="last-name">{{__('Last Name')}}</label>
                                <input class="form-control" type="text" id="last-name" name="last_name"
                                       placeholder="{{__('Last Name')}}">
                            </div>
                            <div class="form-group">
                                <label for="email-address">{{__('Email address')}}</label>
                                <input class="form-control has-validation" data-validation="required|email" type="email"
                                       id="email-address" name="email" placeholder="{{__('Email')}}">
                            </div>
                        <div class="form-group">
                            <label for="mobile-address-reg-form">{{__('Your preffered Studio')}}</label>
                             <select name="preffered_studio" id="preffered_studio" class="form-control has-validation" data-validation="required|preffered_studio">
                             <option value="">Select Type</option>
                             <option value="DJ">DJ</option>
                             <option value="Rehearsal">Rehearsal</option>
                             </select>
                        </div>
                        <div class="form-group">
                            <label for="password-reg-form">{{__('What is your Band/Artist name')}}?</label>
                            <input class="form-control has-validation" data-validation="required|band_or_artist_name"
                                   name="band_or_artist_name" type="text" id="band_or_artist_name"
                                   placeholder="{{__('What is your Band/Artist name')}}?">
                        </div>
                            <div class="form-group">
                            <label for="mobile-address-reg-form">{{__('Mobile Number')}}</label>
                            <input class="form-control has-validation inptFielsd" data-validation="required|phone" type="text"
                                   id="mobile-address-reg-form"  placeholder="{{__('Enter Mobile Number')}}" onkeyup="mobileassign(this)">
                           <input class="form-control has-validation inptFielsd" type="hidden"
                                   id="mobile-address-reg-form-hidden" name="mobile" placeholder="{{__('Enter Mobile Number')}}">
                        </div>
                            <div class="form-group">
                                <label for="password">{{__('Password')}}</label>
                                <input class="form-control has-validation" data-validation="required|min:6:ms"
                                       name="password" type="password" id="password" placeholder="{{__('Password')}}">
                            @if(isset($_GET['ref']))
                               <input type="hidden" name="ref" value="<?= request()->ref ?>"/>
                            @endif
                            </div>
    
                            <div class="form-group">
                                <div class="checkbox checkbox-success">
                                    <input type="checkbox" id="reg-term-condition" name="term_condition" value="1">
                                    <label for="reg-term-condition">
                                        {!! sprintf(__('I accept %s'), '<a href="'. url('/').'/page/9/terms-conditions'.'" class="text-dark">'. __('Terms and Conditions and Privacy Policy') .'</a>') !!}
                                    </label>
                                </div>
                            </div>
                            <div class="form-group mb-0 text-center">
                                <button class="btn btn-primary btn-block text-uppercase" type="submit"> Sign Up</button>
                            </div>
                            <div class="form-message">

                            </div>
                        </form>
                        @if(has_social_login())
                            <div class="text-center">
                                <h5 class="mt-3 text-muted">{{__('Sign up using')}}</h5>
                                <ul class="social-list list-inline mt-3 mb-0">
                                    @if(social_enable('facebook'))
                                        <li class="list-inline-item">
                                            <a href="{{ FacebookLogin::get_inst()->getLoginUrl() }}"
                                               class="social-list-item border-primary text-primary"><i
                                                    class="mdi mdi-facebook"></i></a>
                                        </li>
                                    @endif
                                    @if(social_enable('google'))
                                        <li class="list-inline-item">
                                            <a href="{{ GoogleLogin::get_inst()->getLoginUrl() }}"
                                               class="social-list-item border-danger text-danger"><i
                                                    class="mdi mdi-google"></i></a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        @endif
                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->

                <div class="row mt-3">
                    <div class="col-12 text-center">
                        <div class="d-flex align-items-center justify-content-center">
                            <p class="text-white-50 mr-2">
                                {{__('Back to')}}
                                <a class="text-white mr-1" href="{{url('/')}}">{{__('Home')}}</a>
                            </p>
                            <p class="text-white-50">
                                {{__('Already have account?')}}
                                <a href="{{ url('auth/login') }}" class="text-white ml-1"><b>{{__('Sign In')}}</b></a>
                            </p>
                        </div>

                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
@include('dashboard.components.footer')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/css/intlTelInput.css" integrity="sha512-gxWow8Mo6q6pLa1XH/CcH8JyiSDEtiwJV78E+D+QP0EVasFs8wKXq16G8CLD4CJ2SnonHr4Lm/yY2fSI2+cbmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput.min.js" integrity="sha512-QMUqEPmhXq1f3DnAVdXvu40C8nbTgxvBGvNruP6RFacy3zWKbNTmx7rdQVVM2gkd2auCWhlPYtcW2tHwzso4SA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    var input = document.querySelector("#mobile-address-reg-form");
    window.intlTelInput(input, {
        separateDialCode: true,
         preferredCountries: ['AU'],  
        customPlaceholder: function (
            selectedCountryPlaceholder,
            selectedCountryData
        ) {
            return "e.g. " + selectedCountryPlaceholder;
        },
    });

    input.addEventListener("countrychange",function() {

   console.log("selected country code",jQuery('.iti__selected-dial-code').text()
    );



});

input.addEventListener("countrychange", function() {
  // do something with iti.getSelectedCountryData()
   jQuery('#mobile-address-reg-form-hidden').val(jQuery('.iti__selected-dial-code').text()+jQuery('#mobile-address-reg-form').val())
});

function mobileassign(element)
{
    jQuery('#mobile-address-reg-form-hidden').val(jQuery('.iti__selected-dial-code').text()+jQuery(element).val())

}

</script>