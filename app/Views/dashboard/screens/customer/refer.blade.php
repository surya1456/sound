@include('dashboard.components.header')
<?php
enqueue_style('dropzone-css');
enqueue_script('dropzone-js');
?>
<style>
    
    .ml-10{
    font-size: 13px!important;
    padding-bottom: 21px;
}
    
</style>
<div id="wrapper">
    @include('dashboard.components.top-bar')
    @include('dashboard.components.nav')
    <div class="content-page">
        <div class="content">
            @include('dashboard.components.breadcrumb', ['heading' => __('Profile')])
            {{--Start Content--}}
            <?php
            $current_user = get_current_user_data();
            $user_id = $current_user->getUserId();
            ?>
            <div class="row">
                <div class="col-12 col-lg-4 order-lg-8 ">
                    <div class="profile-preview card relative p-3  bg-pattern-1">
                        <form action="{{ dashboard_url('update-your-avatar') }}" class="form form-action"
                              data-validation-id="form-update-avatar">
                            @include('common.loading')
                            <input type="hidden" name="user_id" value="{{ $user_id }}">
                            <input type="hidden" name="user_encrypt" value="{{ hh_encrypt($user_id) }}">
                            <div class="avatar-preview">
                                <div class="media align-items-center mb-2">
                                    <img src="{{ get_user_avatar($user_id, [85,85]) }}" class="avatar"
                                         alt="{{__('Avatar')}}">
                                    <div class="media-body">
                                        <h4 class="mb-1 mt-3">{{ get_username($user_id) }}</h4>
                                        <p class="text-muted">{{ $current_user->getUserLogin() }}</p>
                                    </div>
                                </div>
                                <a href="javascript:void(0)" class="change-avatar">{{__('Change Avatar')}}</a>
                                <div class="hh-upload-wrapper d-none">
                                    <div class="hh-upload-wrapper clearfix">
                                        <div class="attachments">
                                        </div>
                                        <div class="mt-1">
                                            <a href="javascript:void(0);" class="add-attachment"
                                               title="{{__('Add Image')}}"
                                               data-text="{{__('Add Image')}}"
                                               data-url="{{ dashboard_url('all-media') }}">{{__('Add Image')}}</a>
                                            <input type="hidden" name="avatar" class="upload_value input-upload"
                                                   data-size="85" data-url="{{ dashboard_url('get-attachments') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-left mt-3">
                                <h4 class="font-13 text-uppercase">{{__('About Me:')}}</h4>
                                <p class="text-muted font-13 mb-3">
                                    @if($current_user->description)
                                        {!! balanceTags(nl2br($current_user->description)) !!}
                                    @else
                                        {{__('Nothing :)')}}
                                    @endif
                                </p>
                                <p class="text-muted mb-2 font-13"><strong>{{__('Full Name:')}}</strong>
                                    <span class="ml-2"
                                          data-hh-bind-from="#first_name">{{ $current_user->first_name }}</span>
                                    <span class="ml-1"
                                          data-hh-bind-from="#last_name">{{ $current_user->last_name }}</span>
                                </p>
                                <p class="text-muted mb-2 font-13">
                                    <strong>{{__('Mobile:')}}</strong>
                                    <span class="ml-2">{{ $current_user->mobile }}</span></p>

                                <p class="text-muted mb-2 font-13">
                                    <strong>{{__('Email:')}}</strong>
                                    <span class="ml-2 ">{{ $current_user->email }}</span>
                                </p>

                                <p class="text-muted mb-1 font-13">
                                    <strong>{{__('Location:')}}</strong>
                                    <span class="ml-2">{{ get_country_by_code($current_user->location)['name'] }}</span>
                                </p>
                               
                                
                           
                              
                            </div>
                        </form>
                    </div>

             
                </div>
                <div class="col-12 col-lg-8 order-lg-4">
                    <div class="card relative p-3">
                        <h5>{{__('Refer and Earn Credits')}}</h5>
                        <p class="text-muted mb-1 font-13 ml-10">
                                       <strong>{{__('My Credits')}} </strong>  -
                                     <?php   $total_credit_earning = get_user_meta($user_id,'total_credit_earning'); ?>
                                     <span class="ml-2"> <?= (!empty($total_credit_earning)?'AU$'.$total_credit_earning:'AU$0.00') ?></span>
                                    
                                </p>
                          <p class="text-muted mb-1 font-13 ml-10" >
                                       <strong>{{__('Click to Copy & share')}} </strong> 
                                     <span class="ml-0">
                                       <?php   $userreferalcode = get_user_meta($user_id,'refferal_key'); ?>
                                       <!-- The text field -->
                                        <input type="text" value="<?php echo url('/')?>/auth/sign-up?ref=<?php echo  $userreferalcode ?>" onClick="this.setSelectionRange(0, this.value.length); copylink()"  id="myInput"  class="form-control">
                                        
                                    </span>
                                    
                                </p>
                          <p class="text-muted mb-1 font-13 ml-10">
                                     <span class="ml-0">
                                       <?php   $userreferalcode = get_user_meta($user_id,'refferal_key'); ?>
                                        <a href="sms:?body=I use Soundbox. Click the link to sign up and I get some referral credits :-) :<?php echo url('/')?>/auth/sign-up?ref=<?php echo  $userreferalcode ?>"   class="btn btn-success"    data-action="share/whatsapp/share"  
        target="_blank"><i class="mdi mdi-message" aria-hidden="true"></i> Invite on Sms</a>
                                        
                                    </span>
                                     <span class="ml-0">
                                       <?php   $userreferalcode = get_user_meta($user_id,'refferal_key'); ?>
                                        <a href="mailto:?subject=Check this out, You will love this website&body=I use Soundbox. Click the link to sign up and I get some referral credits :-) :
<?php echo url('/')?>/auth/sign-up?ref=<?php echo  $userreferalcode ?>"  class="btn btn-warning"    data-action="share/whatsapp/share"  
        target="_blank"><i class="mdi mdi-email" aria-hidden="true"></i> Invite on Email</a>
                                        
                                    </span>
                                     <span class="ml-0">
                                       <?php   $userreferalcode = get_user_meta($user_id,'refferal_key'); ?>
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo url('/')?>/auth/sign-up?ref=<?php echo  $userreferalcode ?>&summaryI use Soundbox. Click the link to sign up and I get some referral credits :laughing&display=popup"   class="btn btn-info"   
        target="_blank"><i class="mdi mdi-facebook" aria-hidden="true"></i> Invite on facebook</a>
                                        
                                    </span>
                                    
                                </p>
                                
                                
                                
                                
                               
                    </div>
                </div>
            </div>
            {{--End content--}}
            @include('dashboard.components.footer-content')
        </div>
    </div>
</div>
<script>
    function copylink() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: " + copyText.value);
}
    
</script>
@include('dashboard.components.footer')
