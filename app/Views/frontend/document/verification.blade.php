@include('frontend.components.header')



    <button id="verify-button">Verify</button>
    <script type="text/javascript">
      document.addEventListener('DOMContentLoaded', async () => {

        // Set your publishable key: remember to change this to your live publishable key in production
        // Find your keys here: https://dashboard.stripe.com/apikeys
        const stripe = Stripe('<?= $publish_key ?>');

        var verifyButton = document.getElementById('verify-button');
        verifyButton.addEventListener('click', async () => {
          // Get the VerificationSession client secret using the server-side
          // endpoint you created in step 3.

          try {

            // Create the VerificationSession on the server.
            //const response = await fetch('create-verification-session.php', {
            //  method: 'POST',
           // })
      
      //const response = await fetch('create-verification-session.php');
      const data = <?= $data ?>;
      //console.log(data);
      const client_secret = data.client_secret;
      

      const id = data.id;
      console.log('@clint'+client_secret+"@id"+id);
            // Open the modal on the client.
            const {error} = await stripe.verifyIdentity(client_secret);
            if(!error) {//console.log(response);
              window.location.href = 'submitted/'+id;
            } else {
              alert(error.message);
            }
          } catch(e) {
            alert(e.message);
          }
        })
      })
    </script>
 
 @include('frontend.components.footer')