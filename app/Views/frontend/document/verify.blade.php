@include('frontend.components.header')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

<style>
    img.poa {
    border-right: 2px solid#f24d00;
    border-bottom: 2px solid#3c302a;
}
.drag-area.gf.active img {
    width: 50%;
}
h3.payment-title.xs {
    text-align: center;
    color: #e15928!important;
}
label.text-feild {
    font-weight: 500;
}

.form-calledssa {
    width: 60%;
    margin: auto;
}
button.btn.btn-primary.bttin-xcs {
    width: 100%;
}
img.poa.img-fluid.mx-auto.d-block.rounded {
    margin-left: 0px!important;
}
@media screen and (max-width: 767px) 
{
.form-calledssa {
    width: 100%;
}
}
@media screen and (max-width: 991px) 
{
.form-group.cdc {
    margin-top: 20px;
}
}




.awe-booking .hh-checkout-redirecting .payment-item .payment-detail
{
    margin-top: 0px!important;
}

/*Uplaod file*/

.drag-area{
  border: 2px dashed #999999;
  height:auto;
  width: 100%;
  
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
     
}
.drag-area.active{
  border: 2px solid #fff;
}
.drag-area .icon {
    font-size:35px;
    color: #b3b8bd;
    text-align: center;
    width: 100%;
}
.drag-area header {
  font-size: 13px;
    font-weight: 500;
    color: #000000;
    border-top: 1px solid #b3b8bd;
    border-bottom: 1px solid #b3b8bd;
    margin-top: 10px;
    margin-bottom: 23px;
}
.drag-area span{
  font-size: 25px;
  font-weight: 500;
  color: #fff;
  margin: 10px 0 15px 0;
}
.drag-area button{
    
    padding: 5px 25px;
    font-size: 15px;
    font-weight: 500;
    border: none;
    outline: none;
    background: #fff;
    color: #f24d00;
    border-radius: 5px;
    cursor: pointer;
    background: white;
    border: 1px solid;
    margin-bottom: 20px;

}
.drag-area img{
  height: 100%;
  width: 100%;
  object-fit: cover;
  border-radius: 5px;
}
</style>
<div class="hh-checkout-redirecting pb-5">
    <div class="container">
        <h3 class="payment-title xs"> <i class="fa fa-upload"></i>Verification Document Upload
                    </h3>
                    <div class="sr-root">
      <div class="sr-main">
        <section class="container">
          <div>
            <h1>Verify your identity to book</h1>
            <h4>Get ready to take a photo of your ID and a selfie</h4>

            <button id="verify-button">Verify me</button>
          </div>
        </section>
      </div>
    </div>
        
        <div class="row payment-item" style="padding: 0 50px 50px 50px">
            
             @if(!empty($message))
                 
                 @elseif(!empty($profiledoc) and $profiledoc->is_verified)
                 
                 @else
            <div class="col-lg-12 col-md-12 col-sm-12">
                <!--<img src="http://soundboxstudios.com.au/webdev/storage/u-1/2021/06/16/zz.png" class="poa img-fluid mx-auto d-block rounded">-->
                <h4 class="text-center mb-4">Verify your account so you can make a booking, it only takes 2 minutes.</h4>

            </div>
              @endif
                @if(!empty($message))
                  <div class="col-lg-12 col-md-12 col-sm-12">
                 @elseif(!empty($profiledoc) and $profiledoc->is_verified)
                  <div class="col-lg-12 col-md-12 col-sm-12">
                 @else
            <div class="col-lg-12 col-md-12 col-sm-12">
                @endif
                <div class="xsx">
                    <div class="payment-status">
                  
                    </div>
                @if(\Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
                @endif
                
                 @if(\Session::has('error'))
                <p class="alert  alert-danger">{{ Session::get('error') }}</p>
                @endif

                @if($errors->any())
                    {!! implode('', $errors->all('<p class="alert  alert-danger">:message</p>')) !!}
                @endif
                    
                  
                 @if(!empty($message))
                 <p class="alert  alert-warning">{{$message}}</p>
                     <p style="text-align:center">   <img class="img-fluid" src="<?= url('/') ?>/images/svg/working-progress-svgrepo-com.png" width="600"></p>
                 @elseif(!empty($profiledoc) and $profiledoc->is_verified)
                      <p class="alert  alert-success">Your documents are verified successfully.Feel Free to contact us if any query.</p>
                 @else
                  
                    <div class="payment-detail">
                        
                        
                       <form  action="{{ url('/verification/submit') }}" method="post" enctype="multipart/form-data"  class="form-calledssa "
      >
                            <div class="row"> 
        <div class="form-group cdc col-lg-6 col-md-12 col-sm-12">
         <label for="checkinout" class="text-feild">{{ __('Document type') }}</label>
            <div class="date-wrapper date-double">
                 @csrf
                  <select class="form-control" onchange="selecttime(this)"  required  name="document_type" id="document_type">
                        <option value="">Select document Type</option>
                        <option value="Passport">Passport</option>
                         <option value="Driving License">Driving License</option>
                         <option value="Medicare Card">Medicare Card</option>
                     </select>
            </div>
            
            
            </div>
            
         <div class="form-group col-lg-6 col-md-12 col-sm-12">
          <label for="checkinout" class="text-feild">{{ __('Document Number') }}</label>
            <div class="date-wrapper date-double">
                 <input type="text" name="document_no" required class="form-control"  placeholder="Document Number" /> 
              
            </div>
            
            
            </div>    
            </div>
            
            <div class="row"> 
          <div class="form-group col-lg-12 col-md-12 col-sm-12 fdd">
            <label for="checkinout" class="text-feild ">{{ __('Clear photo of ID') }}</label><br>
            <img class="img-fluid mb-2" src="<?= url('/') ?>/images/_back.jpg" alt="">
            <!--<div class="date-wrapper date-double dropzone">-->
            <!--     <input type="file" name="front_file"  required class="form-control" placeholder="-->
            <!--     Document Number" /> -->
              
            <!--</div>-->
                <div class="drag-area">
                  <div class="icon"><i class="fas fa-cloud-upload-alt"></i></div>
                  <header>Choose a file or drag it here</header>
                  
                  <button type="button" id="front_file_button">Upload</button>
                  <input type="file" name="front_file" accept="image/*" capture="camera"  id="front_file" hidden>
                  <div class="imgfront"></div>
                 </div>
            
            </div>    
        
             <div class="form-group col-lg-12 col-md-12 col-sm-12">
            <label for="checkinout" class="text-feild">{{ __('Clear Selfy with ID') }}</label><br>
            <img class="img-fluid mb-2" src="<?= url('/') ?>/images/_front.jpg" alt="">
            <!--<div class="date-wrapper date-double dropzone">-->
            <!--     <input type="file" name="back_file" required class="form-control"  placeholder="-->
            <!--     Document Number" /> -->
              
            <!--</div>-->
            <div class="drag-area drag-area-bf">
                  <div class="icon"><i class="fas fa-cloud-upload-alt"></i></div>
                  <header>Choose a file or drag it here</header>
                 
                  <button type="button" id="back_file_button">Upload</button>
                  <input type="file" accept="image/*" capture="camera" name="back_file" id="back_file" hidden>
                   <div class="imgback"></div>
            </div>
            
            </div>    
           </div>
            
             <div class="form-group">
                  
               <div class="text-center ">
                       <button 
                           class="btn btn-primary bttin-xcs" >{{__('Submit')}}</button>
                    </div>      
            
            
            </div>
            
          
            
            </form>
                    </div>
                  @endif
                   
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.components.footer')


<script>

    //latest code ends here
    //selecting all required elements
const dropArea = document.querySelector(".drag-area"),
dragText = dropArea.querySelector("header"),
frontbutton = dropArea.querySelector("#front_file_button"),

frontinput = dropArea.querySelector("#front_file");
 //this is a global variable and we'll use it inside multiple functions

frontbutton.onclick = ()=>{
  frontinput.click(); //if user click on the button then the input also clicked
}

frontinput.addEventListener("change", function(){
  //getting user select file and [0] this means if user select multiple files then we'll select only the first one
  file = this.files[0];
  dropArea.classList.add("active");
  showFile(this); //calling function
});


;


//If user Drag File Over DropArea
dropArea.addEventListener("dragover", (event)=>{
  event.preventDefault(); //preventing from default behaviour
  dropArea.classList.add("active");
  dragText.textContent = "Release to Upload File";
});

//If user leave dragged File from DropArea
dropArea.addEventListener("dragleave", ()=>{
  dropArea.classList.remove("active");
  dragText.textContent = "Drag & Drop to Upload File";
});

//If user drop File on DropArea
dropArea.addEventListener("drop", (event)=>{
  event.preventDefault(); //preventing from default behaviour
  //getting user select file and [0] this means if user select multiple files then we'll select only the first one
  file = event.dataTransfer.files[0];
  const dT = new DataTransfer();
  dT.items.add(file);
 
  frontinput.files = dT.files;

  showFile(); //calling function
});


const dropAreaNew = document.querySelector(".drag-area-bf"),
dragTextNew = dropAreaNew.querySelector("header"),

backbutton  = dropAreaNew.querySelector("#back_file_button"),
backinput = dropAreaNew.querySelector("#back_file");
let file; //this is a global variable and we'll use it inside multiple functions




backbutton.onclick = ()=>{
  backinput.click(); //if user click on the button then the input also clicked
}

backinput.addEventListener("change", function(){
  //getting user select file and [0] this means if user select multiple files then we'll select only the first one
  file = this.files[0];
  dropAreaNew.classList.add("active");
  showFileNew(this); //calling function
});


//If user Drag File Over DropArea
dropAreaNew.addEventListener("dragover", (event)=>{
  event.preventDefault(); //preventing from default behaviour
  dropAreaNew.classList.add("active");
  dragTextNew.textContent = "Release to Upload File";
});

//If user leave dragged File from DropArea
dropAreaNew.addEventListener("dragleave", ()=>{
  dropAreaNew.classList.remove("active");
  dragTextNew.textContent = "Drag & Drop to Upload File";
});

//If user drop File on DropArea
dropAreaNew.addEventListener("drop", (event)=>{
  event.preventDefault(); //preventing from default behaviour
  //getting user select file and [0] this means if user select multiple files then we'll select only the first one
  file = event.dataTransfer.files[0];
  //backinput.click();
  const dT = new DataTransfer();
  dT.items.add(file);
 
  backinput.files = dT.files;
  showFileNew(); //calling function
});

function showFileNew(){
  let fileType = file.type; //getting selected file type
  let validExtensions = ["image/jpeg", "image/jpg", "image/png"]; //adding some valid image extensions in array
  if(validExtensions.includes(fileType)){ //if user selected file is an image file
    let fileReader = new FileReader(); //creating new FileReader object
    fileReader.onload = ()=>{
      let fileURL = fileReader.result; //passing user file source in fileURL variable
  	  // UNCOMMENT THIS BELOW LINE. I GOT AN ERROR WHILE UPLOADING THIS POST SO I COMMENTED IT
       let imgTag = `<img src="${fileURL}" alt="image">`; //creating an img tag and passing user selected file source inside src attribute
     // dropAreaNew.innerHTML = imgTag; //adding that created img tag inside
        jQuery('.drag-area-bf').find('.imgback').html(imgTag);
     //dropArea container
    }
    fileReader.readAsDataURL(file);
  }else{
    alert("This is not an Image File!");
    dropAreaNew.classList.remove("active");
    dragTextNew.textContent = "Drag & Drop to Upload File";
  }
}
function showFile(element){
  let fileType = file.type; //getting selected file type
  let validExtensions = ["image/jpeg", "image/jpg", "image/png"]; //adding some valid image extensions in array
  if(validExtensions.includes(fileType)){ //if user selected file is an image file
    let fileReader = new FileReader(); //creating new FileReader object
    fileReader.onload = ()=>{
      let fileURL = fileReader.result; //passing user file source in fileURL variable
  	  // UNCOMMENT THIS BELOW LINE. I GOT AN ERROR WHILE UPLOADING THIS POST SO I COMMENTED IT
       let imgTag = `<img src="${fileURL}" alt="image">`; //creating an img tag and passing user selected file source inside src attribute
      //dropArea.innerHTML = imgTag; //adding that created img tag inside dropArea container
      jQuery('.drag-area').find('.imgfront').html(imgTag);
    }
    fileReader.readAsDataURL(file);
  }else{
    alert("This is not an Image File!");
    dropArea.classList.remove("active");
    dragText.textContent = "Drag & Drop to Upload File";
  }
}
</script>
