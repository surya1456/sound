<?php
global $post;
?>
@include('frontend.components.header')
<?php $banner_bg = get_attachment_url(get_translate($post->thumbnail_id)); ?>



<style>
   .awe-booking .radio label, .awe-booking .checkbox label {
    cursor: pointer;
    font-size: 13px;
    width: 78%;
}
p.cdscs {
    margin-top: 51px;
}
.single-page.single-home.pb-5 .hh-list-of-services {
    width: 90%!important;
}
.page-content-inner ul li {
    list-style: circle;
    margin-left: 29px;
    line-height: 1.9;
    text-align: justify;
}
.text-custom {
  color: #e15928!important
}

.text-gradient {
  background: linear-gradient(45deg, #fb2d49 0%, #fa6340 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
}

.bg-custom {
  background-color: #fa5043;
}

.btn {
  padding: 14px 36px;
  border: 1px solid;
  font-size: 13px;
  -webkit-transition: all 0.5s;
  transition: all 0.5s;
  border-radius: 50px;
  font-weight: 600;
  letter-spacing: 1px;
  text-transform: uppercase;
}

.section {
  padding-top: 50px;
  padding-bottom: 50px;
  position: relative;
}

.bg-overlay {
  background-color: #000;
  opacity: 0.6;
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
}

.bg-gradient {
  background-image: linear-gradient(45deg, #fb2d49 0%, #fa6340 100%);
  opacity: 0.5;
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
}

.f-12 {
  font-size: 12px;
}

.f-13 {
  font-size: 13px;
}

.f-14 {
  font-size: 14px;
}

.f-15 {
  font-size: 15px;
}

.f-16 {
  font-size: 16px;
}

.f-17 {
  font-size: 17px;
}
h3.mb-5.title.text-uppercase.text-custom {
    position: relative;
}

.f-18 {
  font-size: 18px;
}

.f-19 {
  font-size: 19px;
}

.f-20 {
  font-size: 20px;
}

.space {
  padding: 0 6px;
}
h3.mb-5.title.text-uppercase.text-custom {
    margin-bottom: 30px!important;
}
i.fa.fa-microphone {
    /* font-family: FontAwesome; */
    font-size: 24px;
    /* font-weight: 400; */
    /* position: absolute; */
    left: 0;
    width: 100%;
    line-height: 0.5;
    /* text-align: center; */
    /* color: #090909; */
    display: flex;
    padding: 0;
}
div#counter\ counterre {
    margin-top: -27px;
}
.home-desc-icon {
  font-size: 40px;
  position: absolute;
  top: 50%;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  left: 0;
  right: 0;
}

.home-10-bg {
  background-image: -webkit-gradient(linear, left top, right top, color-stop(50%, #feeee4), color-stop(50%, #fef4e4));
  background-image: linear-gradient(to right, #feeee4 50%, #fef4e4 50%);
  background-size: cover;
  padding: 260px 0px 220px 0px;
  background-repeat: no-repeat;
}

.title {
  letter-spacing: 2px;
}


.desc-title {
 font-size: 25px;
    max-width: 100%;
}

.counter-content {
  overflow: hidden;
}

.counter-icon i {
  -webkit-text-stroke: 1px #e15928;
  -webkit-text-fill-color: transparent;
}

.office-add-box {
  position: absolute;
  top: -70px;
  right: 0;
  height: auto;
  width: 272px;
  background-color: #fa5043;
  padding: 38px;
}
@media (min-width: 1600px)
{
.container {
    max-width: 1150px;
}
}
@media (max-width: 991px)
{
.row.align-items-center.xfbgfgf {
    display: none;
}
.row.align-items-center.dscdsc {
    margin-top: 21px;
}
}
@media (min-width: 992px)
{
.row.align-items-center.dscdsc {
    display: none;
}
}
h3.mb-5.title.text-uppercase.text-custom.scdds {
    margin-top: -53px;
}
/*.content-page .content .card.card-box:nth-last-child(1) {*/
/*    display: none;*/
/*}*/
</style>
<div class="page-archive pb-4">
    <div class="banner" style="background: #eee url('{{ $banner_bg }}') center center/cover no-repeat;"></div>
    <div class="container">
        @include('frontend.components.breadcrumb', ['currentPage' => get_translate($post->post_title)])
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="page-content">
                    <!--<h1 class="title">{{ get_translate($post->post_title) }}</h1>-->
                    <div class="page-content-inner">
                        <?php echo balanceTags(get_translate($post->post_content)); ?>
                    </div>
                </div>
            </div>
            <!--<div class="col-12 col-lg-3">-->
            <!--    <div class="page-sidebar">-->
            <!--        @include('frontend.components.sidebar', ['type' => 'page'])-->
            <!--    </div>-->
            <!--</div>-->
        </div>
    </div>
</div>
@include('frontend.components.footer')
