<?php
global $post;
?>
<style>
.appointmentCalendar{box-shadow:none!important}.appointmentCalendarDays{display:table;width:100%;border-collapse:collapse}.appointmentCalendar .calendarWeek,.appointmentCalendar .daysOfWeek{display:table-row}.appointmentCalendar .daysOfWeek{border-bottom:6px solid transparent}.appointmentCalendar .calendarDay,.appointmentCalendar .dayOfWeek{display:table-cell;font-size:12px;text-align:center}.appointmentCalendar .dayOfWeek{line-height:46px;background-color:#eef3ff;color:#527ffe;text-align:center;width:calc(100% / 7)}.appointmentCalendar .calendarDay{position:relative;color:#000}.appointmentCalendar .calendarDay:after{content:"";width:6px;height:6px;border-radius:50%;background-color:#4c72fb;position:absolute;left:50%;transform:translateX(-50%);bottom:6px;opacity:0;transition:.3s}.appointmentCalendar .calendarDayEach{display:block;line-height:36px;border:1px solid rgba(255,255,255,0);cursor:pointer;margin:1px;pointer-events:none}.appointmentCalendar .calendarDay.isActive .calendarDayEach{background-color:#4c72fb;color:#fff}.appointmentCalendar .calendarDay.isActive .calendarDayEach,.appointmentCalendar .calendarDay.isToday .calendarDayEach,.appointmentCalendar .calendarDay:not(.empty):hover .calendarDayEach{transition:.3s;border-radius:4px;border:1px solid #97adfd}.appointmentCalendar .calendarDay.isActive .calendarDayEach,.appointmentCalendar .calendarDay:not(.empty):hover .calendarDayEach{border-color:#4c72fb}.appointmentCalendar .calendarDay.isSelected:after{opacity:1}.appointmentCalendar .calendarDay.isSelected.isActive:after{background-color:#fff}.appointmentCalendar .calendarDay.isUnavailable{color:#cad0dd;pointer-events:none}.appointmentCalendarContainer{border-radius:6px;border:1px solid #c3cad8;background-color:#fff}.appointmentCalendarContainer .currentDate{font-size:16px;padding:10px 30px 10px 12px;border:none;width:100%;background-image:url("data:image/svg+xml,%3Csvg width='18' height='18' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill-rule='nonzero' fill='none'%3E%3Cpath d='M18 5.625v11.28c0 .604-.504 1.095-1.125 1.095H1.125C.504 18 0 17.51 0 16.904V5.625h18zM4.732 12.844H2.68l-.101.009a.563.563 0 00-.426.357l-.026.095-.01.101v1.969l.01.101a.563.563 0 00.357.426l.095.026.101.01h2.052l.1-.01a.563.563 0 00.427-.357l.026-.095.01-.101v-1.969l-.01-.1a.564.564 0 00-.357-.427l-.095-.026-.101-.01zm5.294 0H7.974l-.1.009a.563.563 0 00-.427.357l-.026.095-.01.101v1.969l.01.101a.563.563 0 00.357.426l.095.026.101.01h2.052l.1-.01a.563.563 0 00.427-.357l.026-.095.01-.101v-1.969l-.01-.1a.564.564 0 00-.357-.427l-.095-.026-.101-.01zM4.732 7.687H2.68l-.101.01a.563.563 0 00-.426.357l-.026.095-.01.101v1.969l.01.1a.563.563 0 00.357.427l.095.026.101.01h2.052l.1-.01a.563.563 0 00.427-.357l.026-.095.01-.101V8.25l-.01-.101a.563.563 0 00-.357-.426l-.095-.026-.101-.01zm5.294 0H7.974l-.1.01a.563.563 0 00-.427.357l-.026.095-.01.101v1.969l.01.1a.563.563 0 00.357.427l.095.026.101.01h2.052l.1-.01a.563.563 0 00.427-.357l.026-.095.01-.101V8.25l-.01-.101a.563.563 0 00-.357-.426l-.095-.026-.101-.01zm5.294 0h-2.052l-.1.01a.563.563 0 00-.427.357l-.026.095-.01.101v1.969l.01.1a.563.563 0 00.357.427l.095.026.101.01h2.052l.1-.01a.563.563 0 00.427-.357l.026-.095.01-.101V8.25l-.01-.101a.563.563 0 00-.357-.426l-.095-.026-.101-.01z' fill='%2359647C'/%3E%3Cpath d='M14.625 1.266h2.25C17.496 1.266 18 1.8 18 2.46V4.5H0V2.46c0-.66.504-1.194 1.125-1.194h2.25v-.07C3.375.535 3.879 0 4.5 0s1.125.535 1.125 1.195v.07h6.75v-.07C12.375.535 12.879 0 13.5 0s1.125.535 1.125 1.195v.07z' fill='%2359647C'/%3E%3C/g%3E%3C/svg%3E");background-color:transparent;background-repeat:no-repeat;background-position:calc(100% - 13px);background-size:16px}.appointmentCalendarContainer .monthYearPicker{display:flex;border-top:1px solid #c3cad8;border-bottom:1px solid #c3cad8}.appointmentCalendarContainer .monthYearPicker .pickerArrow{position:absolute;right:0;width:40px;z-index:2;opacity:.3;cursor:pointer;transition:.3s;border-color:transparent;border-width:0;background-color:rgba(0,0,0,0);padding:0;outline:0}.appointmentCalendarContainer .monthYearPicker .pickerArrow:hover{opacity:1}.appointmentCalendarContainer .monthYearPicker .pickerArrow.prev{top:0;bottom:50%}.appointmentCalendarContainer .monthYearPicker .pickerArrow.prev:after{top:3px;background-image:url("data:image/svg+xml,%3Csvg width='10' height='6' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M8.599 5.337a.837.837 0 001.158-.02.834.834 0 000-1.179l-.667-.623-1.434-1.34L5.57.226a.837.837 0 00-1.136 0L.263 4.118c-.343.341-.35.87-.02 1.199a.832.832 0 001.155.02L5 1.975l3.599 3.362z' fill='%23000' fill-rule='nonzero'/%3E%3C/svg%3E")}.appointmentCalendarContainer .monthYearPicker .pickerArrow.next{top:50%;bottom:0}.appointmentCalendarContainer .monthYearPicker .pickerArrow.next:after{top:-3px;background-image:url("data:image/svg+xml,%3Csvg width='10' height='6' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M1.403.227a.837.837 0 00-1.158.02.834.834 0 000 1.178l.666.623 1.435 1.34 2.087 1.95a.837.837 0 001.136 0l4.17-3.893c.343-.34.35-.87.02-1.198a.832.832 0 00-1.155-.02L5.002 3.588 1.403.227z' fill='%23000' fill-rule='nonzero'/%3E%3C/svg%3E")}.appointmentCalendarContainer .monthYearPicker .pickerArrow:after{content:"";position:absolute;top:0;left:0;width:100%;height:100%;background-repeat:no-repeat;background-position:center;pointer-events:none}.appointmentCalendarContainer .monthYearPicker .pickerItem{width:50%;position:relative}.appointmentCalendarContainer .monthYearPicker .pickerItem+.pickerItem{border-left:1px solid #c3cad8}.appointmentCalendarContainer .monthYearPicker .pickerItem select{appearance:none;-webkit-appearance:none;width:100%;height:45px;margin:0;border-width:0;background-color:transparent;font-size:16px;padding-left:12px}.appointmentCalendarContainer .monthYearPicker .pickerItem select::-ms-expand{display:none}#jfQuestion-proxy .appointmentField,.jfCard .appointmentField{width:100%}.stageContainer .appointmentFieldWrapper .appointmentSlot{pointer-events:none}.appointmentFieldWrapper[data-breakpoint=isLarge] .appointmentDates{max-width:460px}.appointmentFieldWrapper[data-breakpoint=shouldBreakIntoNewLine] .appointmentFieldRow{flex-direction:column}.appointmentFieldWrapper[data-breakpoint=shouldBreakIntoNewLine] .appointmentFieldRow.forCalendar{flex-direction:column}.appointmentFieldWrapper[data-breakpoint=shouldBreakIntoNewLine] .appointmentFieldRow.forSelectedDate{flex-direction:row;align-items:center}.appointmentFieldWrapper[data-breakpoint=shouldBreakIntoNewLine] .appointmentCalendar{max-width:324px;width:100%}.appointmentFieldWrapper[data-breakpoint=shouldBreakIntoNewLine] .appointmentDates{max-width:324px;margin-top:16px;width:100%;margin-left:0}.appointmentFieldWrapper[data-breakpoint=shouldBreakIntoNewLine] .appointmentSlots{height:auto}.appointmentFieldWrapper[data-breakpoint=shouldBreakIntoNewLine]:not(.isOpenMobile) .appointmentFieldContainer .appointmentCalendarDays,.appointmentFieldWrapper[data-breakpoint=shouldBreakIntoNewLine]:not(.isOpenMobile) .appointmentFieldContainer .monthYearPicker{display:none}.appointmentFieldWrapper[data-breakpoint=shouldBreakIntoNewLine]:not(.isOpenMobile) .appointmentFieldRow.forCalendar{flex-direction:column}.appointmentFieldRow.forCalendar{display:flex;justify-content:flex-start;flex-wrap:wrap}.form-label-left+.form-input .appointmentFieldRow.forCalendar,.form-label-right+.form-input .appointmentFieldRow.forCalendar{max-width:310px;flex-direction:column}.form-label-left+.form-input .appointmentFieldRow.forCalendar .appointmentDates,.form-label-right+.form-input .appointmentFieldRow.forCalendar .appointmentDates{margin-top:16px;width:100%}.form-label-left+.form-input .appointmentFieldRow.forCalendar .appointmentSlots,.form-label-right+.form-input .appointmentFieldRow.forCalendar .appointmentSlots{height:auto;max-height:340px;overflow:auto}.appointmentFieldRow.forTimezonePicker{display:inline-block}.appointmentFieldRow.forSelectedDate{font-weight:400;font-size:13px;color:#fff;background-color:#4c72fb;background-image:url("data:image/svg+xml,%3Csvg width='20' height='20' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='none' fill-rule='evenodd'%3E%3Cg fill='%23FFF'%3E%3Cpath d='M3.5 10.4a3.1 3.1 0 100 6.2 3.1 3.1 0 000-6.2zm1.394 2.009h0zm-1.682.55l1.116-1.116h0a.83.83 0 011.174 1.173L3.798 14.72a.83.83 0 01-1.173 0l-.782-.782a.83.83 0 011.173-1.174l.196.196z' stroke='%23FFF' stroke-width='.8'/%3E%3Cpath d='M17.852 1.89h-2.049v-.6c0-.712-.578-1.29-1.289-1.29-.71 0-1.289.578-1.289 1.29v.6H8.006v-.6C8.006.577 7.428 0 6.717 0c-.71 0-1.289.578-1.289 1.29v.617a.429.429 0 00-.114-.016H3.602a2.15 2.15 0 00-2.149 2.148v5.32a.43.43 0 00.86 0V6.315h16.904a.43.43 0 000-.859H2.313V4.039c0-.71.578-1.289 1.289-1.289h1.712a.43.43 0 00.114-.016v.553a1.29 1.29 0 002.578 0V2.75h5.22v.537c0 .71.578 1.29 1.288 1.29.711 0 1.29-.58 1.29-1.29V2.75h2.048c.71 0 1.289.578 1.289 1.29v14.327a.775.775 0 01-.774.774H3.083a.774.774 0 01-.774-.774v-1.112c0-.205-.192-.43-.43-.43a.43.43 0 00-.429.43v1.112c0 .9.732 1.633 1.633 1.633h15.284c.9 0 1.633-.733 1.633-1.633V5.882 4.039a2.15 2.15 0 00-2.148-2.148zM7.147 3.288a.43.43 0 01-.86 0V1.29a.43.43 0 01.86 0v1.998zm7.797 0a.43.43 0 01-.86 0V1.29a.43.43 0 01.86 0v1.998z' fill-rule='nonzero'/%3E%3Cpath d='M10.721 12.266c-.9 0-1.632.732-1.632 1.632 0 .9.732 1.633 1.632 1.633.9 0 1.633-.732 1.633-1.633 0-.9-.732-1.632-1.633-1.632zm0 2.406a.774.774 0 010-1.547.774.774 0 010 1.547zM15.987 12.266c-.9 0-1.633.732-1.633 1.632 0 .9.733 1.633 1.633 1.633s1.633-.732 1.633-1.633c0-.9-.733-1.632-1.633-1.632zm0 2.406a.774.774 0 010-1.547.774.774 0 010 1.547zM5.711 7c-.9 0-1.633.732-1.633 1.633 0 .9.733 1.633 1.633 1.633s1.633-.733 1.633-1.633S6.61 7 5.71 7zm0 2.406a.774.774 0 010-1.547.774.774 0 010 1.547zM16.242 7c-.9 0-1.633.732-1.633 1.633 0 .9.733 1.633 1.633 1.633s1.633-.733 1.633-1.633S17.143 7 16.242 7zm0 2.406a.774.774 0 010-1.547.774.774 0 010 1.547zM10.977 7c-.9 0-1.633.732-1.633 1.633 0 .9.732 1.633 1.633 1.633.9 0 1.633-.733 1.633-1.633S11.877 7 10.977 7zm0 2.406a.774.774 0 010-1.547.774.774 0 010 1.547z' fill-rule='nonzero'/%3E%3Cpath d='M13.299 2l.001 1.25c0 .647.492 1.18 1.122 1.244l.128.006a1.25 1.25 0 001.244-1.122l.006-.128L15.799 2H17.5a2 2 0 012 2v2H2V4a2 2 0 012-2l1.5-.001V3.25c0 .647.492 1.18 1.122 1.244l.128.006C7.44 4.5 8 3.94 8 3.25V1.999L13.299 2z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E");background-repeat:no-repeat;background-position:6px center;padding:8px 8px 8px 34px;border-radius:4px;margin-top:12px;display:flex;align-content:center}.appointmentFieldRow.forSelectedDate span{flex-grow:1}.appointmentFieldRow.forSelectedDate button{flex-shrink:1;width:18px;height:18px;background-color:transparent;border-width:0;outline:0;background-image:url("data:image/svg+xml,%3Csvg width='18' height='18' viewBox='0 0 18 18' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='none' fill-rule='evenodd'%3E%3Ccircle fill='%23FFF' cx='9' cy='9' r='9'/%3E%3Cpath d='M9.59 9l2.946-2.946a.417.417 0 00-.59-.59L9 8.411 6.054 5.464a.417.417 0 00-.59.59L8.411 9l-2.947 2.946a.417.417 0 00.59.59L9 9.589l2.946 2.947a.417.417 0 00.59-.59L9.589 9z' stroke='%234C72FB' fill='%23000'/%3E%3C/g%3E%3C/svg%3E");background-position:center;background-repeat:no-repeat;background-size:18px;color:rgba(255,255,255,0);cursor:pointer}.form-line-error .appointmentFieldRow.forSelectedDate{display:none}.appointmentFieldInput{display:none}.appointmentFieldContainer .appointmentCalendar{width:100%;max-width:312px}.appointmentFieldContainer .appointmentDates{margin-left:12px;width:calc(100% - 324px);display:flex;flex-direction:column}.appointmentFieldContainer *{box-sizing:border-box}@media (max-width:500px){.appointmentFieldContainer.appointmentFieldMobile:not(.isOpenMobile) .appointmentCalendarDays,.appointmentFieldContainer.appointmentFieldMobile:not(.isOpenMobile) .monthYearPicker{display:none}}.appointmentField .timezonePicker{position:absolute;top:0;left:0;width:100%;height:100%;opacity:0;z-index:3}.appointmentField .timezonePicker:hover+.timezonePickerName{background-color:#eef3ff;color:#2c3345}.appointmentField .timezonePickerWrapper{display:inline-block;position:relative}.appointmentField .timezonePickerName{display:inline-flex;align-items:center;color:inherit;font-size:12px;padding:4px;pointer-events:none;transition:.3s;border-radius:15px}.appointmentField .timezonePickerName:after,.appointmentField .timezonePickerName:before{content:"";display:inline-block}.appointmentField .timezonePickerName:before{margin-right:7px;width:16px;height:16px;background-image:url("data:image/svg+xml,%3Csvg width='16' height='16' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23000' fill-rule='nonzero'%3E%3Cpath d='M7.952 0C3.558 0 0 3.566 0 7.96s3.558 7.96 7.952 7.96c4.402 0 7.968-3.566 7.968-7.96S12.354 0 7.952 0zm.008 14.328A6.366 6.366 0 011.592 7.96 6.366 6.366 0 017.96 1.592a6.366 6.366 0 016.368 6.368 6.366 6.366 0 01-6.368 6.368z'/%3E%3Cpath d='M8.358 3.98H7.164v4.776l4.18 2.508.596-.98L8.358 8.16z'/%3E%3C/g%3E%3C/svg%3E")}.appointmentField .timezonePickerName:after{margin-left:5px;margin-right:2px;width:9px;align-self:flex-end;background-image:url("data:image/svg+xml,%3Csvg width='9' height='6' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill='%23000' d='M4.816 5.38L8.498.646A.4.4 0 008.182 0H.818a.4.4 0 00-.316.646L4.184 5.38a.4.4 0 00.632 0z' fill-rule='evenodd'/%3E%3C/svg%3E");height:10px;background-repeat:no-repeat}.jfCard .appointmentField .timezonePickerName:after{height:13px}.appointmentDate{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;padding-right:12px;font-size:16px;flex-grow:1}.appointmentDateSelect{display:flex;align-items:center}.appointmentDayPicker{flex-shrink:1;display:flex}.appointmentDayPickerButton{color:rgba(255,255,255,0);width:26px;height:26px;border-width:0;border-radius:50%;background-color:#ecedf3;background-image:url("data:image/svg+xml,%3Csvg width='8' height='14' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M1.1 1.628L6.115 7 1.099 12.37a.376.376 0 00.54.521L6.9 7.256a.377.377 0 00-.001-.51L1.648 1.118a.378.378 0 00-.54-.01.373.373 0 00-.007.52z' fill='%238894AC' stroke='%238894AC' stroke-width='1.5' fill-rule='evenodd'/%3E%3C/svg%3E");background-repeat:no-repeat;background-position:10px center;transition:.3s}.appointmentDayPickerButton:hover{background-color:#dcdee9}.appointmentDayPickerButton+.appointmentDayPickerButton{margin-left:12px}.appointmentDayPickerButton.prev{transform:rotate(180deg)}.appointmentSlots{flex-grow:1;position:relative;flex:1 1 auto;height:0;margin:16px 0 8px;font-size:14px;min-height:217px}.appointmentSlots-empty{display:flex;width:100%;height:100%;flex:1 1 auto;align-items:center;justify-content:center;flex-direction:column}.appointmentSlots-empty-container{padding:24px;border-radius:6px;background-color:#fff}.appointmentSlots-empty-icon{width:124px;height:102px}.appointmentSlots-empty-text{margin-top:8px;font-size:14px;color:#8894ac}.appointmentSlotsContainer{display:flex;justify-content:space-between;align-content:flex-start;flex-wrap:wrap;height:100%}.appointmentSlot{height:44px;display:flex;align-items:center;justify-content:center;width:calc(17% - 6px);border:1px solid #4c72fb;background-color:#fff;color:#4c72fb;border-radius:6px;text-align:center;font-size:14px;margin-bottom:8px;transition:.3s;cursor:pointer}.appointmentSlot:not(.disabled):not(.active):hover{background-color:#eef3ff}.appointmentSlot.active{animation:indicate .2s linear forwards}.appointmentSlot.disabled{border:1px solid #c3cad8;background-color:#f4f5f8;color:#c3cad8;cursor:default}@media (max-width:740px){#jfQuestion-proxy .appointmentCalendar,.jfCard .appointmentCalendar{margin:auto}#jfQuestion-proxy .appointmentCalendarContainer,#jfQuestion-proxy .appointmentCalendarTimezone,.jfCard .appointmentCalendarContainer,.jfCard .appointmentCalendarTimezone{width:100%;max-width:324px}#jfQuestion-proxy .appointmentCalendarTimezone,.jfCard .appointmentCalendarTimezone{order:-1;margin-top:-8px}#jfQuestion-proxy .appointmentDates,.jfCard .appointmentDates{width:100%;max-width:324px;margin-top:16px;margin-left:auto!important;margin-right:auto!important}#jfQuestion-proxy .appointmentCalendarContainer,#jfQuestion-proxy .appointmentCalendarTimezone,.jfCard .appointmentCalendarContainer,.jfCard .appointmentCalendarTimezone{width:100%;max-width:324px}#jfQuestion-proxy .appointmentSlots,.jfCard .appointmentSlots{min-height:auto;height:auto}#jfQuestion-proxy .appointmentFieldWrapper:not(.isOpenMobile) .appointmentCalendarDays,#jfQuestion-proxy .appointmentFieldWrapper:not(.isOpenMobile) .monthYearPicker,.jfCard .appointmentFieldWrapper:not(.isOpenMobile) .appointmentCalendarDays,.jfCard .appointmentFieldWrapper:not(.isOpenMobile) .monthYearPicker{display:none}#jfQuestion-proxy .appointmentFieldRow.forCalendar,.jfCard .appointmentFieldRow.forCalendar{flex-direction:column}#jfQuestion-proxy .appointmentFieldRow.forSelectedDate,.jfCard .appointmentFieldRow.forSelectedDate{margin-top:0}}@media (max-width:600px){.jotform-form .appointmentCalendarContainer,.jotform-form .appointmentCalendarTimezone{width:100%;max-width:324px}.jotform-form .appointmentCalendarTimezone{order:-1;margin-top:-8px}.jotform-form .appointmentSlots{height:auto}.jotform-form .appointmentFieldWrapper:not(.isOpenMobile) .appointmentCalendarDays,.jotform-form .appointmentFieldWrapper:not(.isOpenMobile) .monthYearPicker{display:none}.jotform-form .appointmentFieldRow.forCalendar{flex-direction:column}.jotform-form .appointmentFieldWrapper[data-breakpoint=isNormal] .appointmentDates{margin-left:0;width:324px;margin-top:16px}}@keyframes indicate{0%{color:#4c72fb;background-color:transparent}100%{color:#fff;background-color:#4c72fb}}#duration{width:8rem;border:none}.slot{border:1px solid #ccc;background-color:#fff;background:#ccc;color:#000;cursor:pointer}.available{border:1px solid #4c72fb;background:#fff;color:#000;cursor:pointer;}.form-group.mt-2.bg-warning { padding: 10px;}
</style>


<form class="form-action" name="_booking_main_form" action="{{ url('add-to-cart-home') }}" method="post" data-loading-from=".form-body"
      data-validation-id="form-add-cart">
    @if($post->booking_type == 'per_night')
        <div class="form-group">
            <label for="checkinout">{{ __('Check In/Out') }}</label>
            <div class="date-wrapper date-double"
                 data-date-format="{{ hh_date_format_moment() }}"
                 data-action="{{ url('get-home-availability-single') }}">
                <input type="text" class="input-hidden check-in-out-field"
                       name="checkInOut" data-home-id="{{ $post->post_id }}"
                       data-home-encrypt="{{ hh_encrypt($post->post_id) }}">
                <input type="text" class="input-hidden check-in-field"
                       name="checkIn">
                <input type="text" class="input-hidden check-out-field"
                       name="checkOut">
                <span class="check-in-render"
                      data-date-format="{{ hh_date_format_moment() }}" onchange="availabletimes(this)" ></span>
                <span class="divider"></span>
                <span class="check-out-render"
                      data-date-format="{{ hh_date_format_moment() }}"></span>
            </div>
        </div>
    @elseif($post->booking_type == 'per_hour')
        <div class="form-group">
            <label for="checkinout">{{__('Find a Session ')}}</label>
            <div class="date-wrapper date-single"
                 data-date-format="{{ hh_date_format_moment() }}"
                 data-action-time="{{ url('get-home-availability-time-single') }}"
                 data-action="{{ url('get-home-availability-single') }}">
                <input type="text"
                       class="input-hidden check-in-out-single-field"
                       name="checkInOut" data-home-id="{{ $post->post_id }}"
                       data-home-encrypt="{{ hh_encrypt($post->post_id) }}">
                <input type="text" class="input-hidden check-in-field"
                       name="checkIn">
                <input type="text" class="input-hidden check-out-field"
                       name="checkOut">
                <span class="check-in-render" id="datechange"
                      data-date-format="{{ hh_date_format_moment() }}"></span>
            </div>
        </div>
        <div class="form-group form-group-date-time d-none" id="_date_and_time" style="display: none;">
            <label>{{ __('Time') }}</label>
            <div class="date-wrapper date-time">
                <div class="date-render check-in-render"
                     data-time-format="{{ hh_time_format() }}">
                    <div class="render">{{__('Start Time')}}</div>
                    <div class="dropdown-time">

                    </div>
                    <input type="hidden" name="startTime" value=""
                           class="input-checkin" onchange="selecttime(this,false);slectedtimehourmg(this)"/>
                </div>
                <span class="divider"></span>
                  <div 
                    >
                    
                  <select class="form-control" onchange="selecttime(this)"  name="duration" id="duration" disabled>
                      <option value="" >Select Duration</option>
                      <?php for($start=1;$start<=24;$start++): ?>
                             <option value="<?= $start ?>"><?= $start ?> <?=($start>1?'Hours':'Hour') ?></option>
                      <?php endfor;?>
                     </select>
                    
                    </div>
                <div class="date-render check-out-render"
                     data-time-format="{{ hh_time_format() }}" style="display:none">
                    <div class="render">{{__('End Time')}}</div>
                    <div class="dropdown-time">

                    </div>
                    <input type="hidden" name="endTime" value=""
                           class="input-checkin"/>
                </div>
            </div>
            
            
            <div class="appointmentDates"  style="display:none">
           <?php $arrayoftime =['12 AM','1 AM','2 AM','3 AM','4 AM','5 AM','6 AM','7 AM','8 AM','9 AM','10 AM','11 AM','12 PM','1 PM','2 PM','3 PM','4 PM','5 PM','6 PM','7 PM','8 PM','9 PM','10 PM','11 PM']; ?>
           <?php $peakhours = \DB::table('on_off_peak_hours_price')->where(array('room_id'=>$post->post_id))->first();
            $home_object = new \App\Models\Home();
           $post_item = $home_object->getById($post->post_id);
           $weekprice = $post_item->weekend_price;
          // print_r($peakhours);
            $start = date('Y-m-d').' '.date("H:i:00",strtotime($peakhours->start_time_on_peak));
            $end    = date('Y-m-d').' '.date("H:i:00",strtotime($peakhours->end_time_on_peak));
        
            $begin = new DateTime( $start);
            $end = new DateTime( $end );
            
            $timeRanges = [];
           

            $period = new DatePeriod(
    new DateTime( date("H:i:00",strtotime($peakhours->start_time_on_peak))),
    new DateInterval('PT1H'),
    new DateTime(date("H:i:00",strtotime($peakhours->end_time_on_peak)))
);
        $peakshours =array();
           foreach ($period as $date) {
             $peakshours[] =$date->format("H:i");
}  
         
          
           ?> 
           <div class="appointmentSlots slots ">
              <div class="appointmentSlotsContainer">
                 <?php foreach($arrayoftime as $key=> $time):
                    
                    /*** check if peak or not */
                    $toptipmessage  = 'Off Peak Hour AU$'.$post->base_price;
                    if(in_array(date("H:i",strtotime($time)),$peakshours))
                    {  
                      
                            $toptipmessage  = 'Peak Hour AU$'.$peakhours->on_peak_price;
                    }
                     

                    
                    ?>
                <div class="appointmentSlot slot " data-toggle="tooltip" data-placement="top" title="<?= $toptipmessage  ?>"  id="<?= $key+1 ?>" data-value="<?= $time ?>"><?= $time ?></div>
                <?php endforeach; ?>
              
              </div>
            </div>
              <div style="padding:10px;    margin-left: 2px;float: left;
    
    margin-right: 10px; display:block!important;    border: 1px solid #ccc;
    background-color: #fff;
    background: #ccc;"></div>
    <div class="selectedhourtext">Hour unavailable.</div>
          </div>
        </div>
    @endif
    <?php
    $max_guest = (int)$post->number_of_guest;
    ?>
    <!--div class="form-group">
        <label for="guest">{{__('Guests')}}</label>
        <div
            class="guest-group @if($post->enable_extra_guest == 'on') has-extra-guest @endif">
            <button type="button" class="btn btn-light dropdown-toggle"
                    data-toggle="dropdown"
                    data-text-guest="{{__('Guest')}}"
                    data-text-guests="{{__('Guests')}}"
                    data-text-infant="{{__('Infant')}}"
                    data-text-infants="{{__('Infants')}}"
                    aria-haspopup="true" aria-expanded="false">
                &nbsp;
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="group">
                    <span class="pull-left">{{__('Adults')}}</span>
                    <div class="control-item">
                        <i class="decrease ti-minus"></i>
                        <input type="number" min="1" step="1"
                               max="{{ $max_guest }}"
                               name="num_adults"
                               value="4">
                        <i class="increase ti-plus"></i>
                    </div>
                </div>
                <div class="group">
                    <span class="pull-left">{{__('Children')}}</span>
                    <div class="control-item">
                        <i class="decrease ti-minus"></i>
                        <input type="number" min="0" step="1"
                               max="{{ $max_guest }}"
                               name="num_children"
                               value="0">
                        <i class="increase ti-plus"></i>
                    </div>
                </div>
                <div class="group">
                    <span class="pull-left">{{__('Infants')}}</span>
                    <div class="control-item">
                        <i class="decrease ti-minus"></i>
                        <input type="number" min="0" step="1"
                               max="{{ $max_guest }}"
                               name="num_infants"
                               value="0">
                        <i class="increase ti-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <div class="form-group">
        <?php
        $requiredExtra = $post->required_extra;
        ?>
        @if ($requiredExtra)
            <div class="extra-services">
                <label class="d-block mb-2" for="extra-services">
                    {{__('Extra')}}
                    <span class="text-danger f12">{{__('(required)')}}</span>
                    <a href="#extra-collapse" class="right"
                       data-toggle="collapse">{!! get_icon('002_download_1', '#2a2a2a','15px') !!}</a>
                </label>
                <div id="extra-collapse" class="collapse show">
                    @foreach ($requiredExtra as $ex)
                        <div class="extra-item d-flex">
                            <span
                                class="name">{{ get_translate($ex['name']) }}</span>
                            <span
                                class="price ml-auto">{{ convert_price($ex['price']) }}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        <?php
        $extra = $post->not_required_extra;
        ?>
        @if ($extra)
            <div class="extra-services">
                <label class="d-block mb-2" for="extra-services">
                    <span>{{__('Extra (optional)')}}</span>
                    <a href="#extra-not-required-collapse" class="right"
                       data-toggle="collapse">{!! get_icon('002_download_1', '#2a2a2a','15px') !!}</a>
                </label>
                <div id="extra-not-required-collapse" class="collapse">
                    @foreach ($extra as $ex)
                        <div class="extra-item d-flex">
                            <div class="checkbox checkbox-success">
                                <input
                                    id="extra-service-{{ $ex['name_unique'] }}"
                                    class="input-extra"
                                    type="checkbox" name="extraServices[]"
                                    value="{{ $ex['name_unique'] }}">
                                <label
                                    for="extra-service-{{ $ex['name_unique'] }}">
                                    <span
                                        class="name">{{ get_translate($ex['name']) }}</span>
                                </label>
                            </div>
                            <span
                                class="price ml-auto">{{ convert_price($ex['price']) }}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
    <div class="form-group form-render">
    </div>
    
    <div class="form-group mt-2 ">
        <input type="hidden" name="homeID" value="{{ $post->post_id }}">
        <input type="hidden" name="homeEncrypt"
               value="{{ hh_encrypt($post->post_id) }}">
        <input type="submit" id="_booking_btn" style="display: none;" class="btn btn-primary btn-block text-uppercase"
               name="sm"
               value="{{__('Book Now')}}">
    </div>
     
    <div class="form-message"></div>
    @csrf
</form>

<script>
    const convertTime12to24 = (time12h) => {
  const [time, modifier] = time12h.split(' ');

  let [hours, minutes] = time.split(':');

  if (hours === '12') {
    hours = '00';
  }

  if (modifier === 'PM') {
    hours = parseInt(hours, 10) + 12;
  }

  return `${hours}`;
}


   function selecttime(element,ajaxrequestsend=true)
    {
        $('.form-render').hide();
          console.log("selected time",jQuery(element).val());
         var element = '#duration';
          if(jQuery(element).val()=='')
          {
              return false;
          }
          
          
          var selectedtime = jQuery('input[name="startTime"]').val();
          
          var timetouse    = selectedtime.split(' ');
          console.log('split time ',timetouse)
          
          var makeselectedhours = [];
          jQuery('.slot').removeClass('active');
          var endtime ='';
          var starttime = parseInt(convertTime12to24(selectedtime))>11?parseInt(convertTime12to24(selectedtime))+parseInt(1):parseInt(convertTime12to24(selectedtime));
          var hours   = parseInt(convertTime12to24(selectedtime))<=11?parseInt(jQuery(element).val() ):parseInt(jQuery(element).val() )-1;
            
          console.log("content comes here bro",parseInt(starttime)+hours)
          for(var start=parseInt(convertTime12to24(selectedtime))<=11?starttime+parseInt(1):starttime; start<=parseInt(starttime)+hours; start++ )
          {
              if(!jQuery( '#'+start).hasClass( "available" ))
              { 
                  
              }
              else
              {
                jQuery('#'+start).addClass('active');
                endtime = jQuery('#'+(start+1)).text();
              }
          }

          jQuery('.slot').hide();
          
          if(parseInt(convertTime12to24(selectedtime))>2)
          {
            
             var first  = parseInt(convertTime12to24(selectedtime))-1;
             var second = parseInt(convertTime12to24(selectedtime))-2;
             jQuery('#'+first).show();
             jQuery('#'+second).show();
               
          }
          
           if(parseInt(convertTime12to24(selectedtime))<=1)
          {
             console.log("coming here to see if i can check it in higher selection ");
             var first  = parseInt(convertTime12to24(selectedtime))+24;
             var second = parseInt(convertTime12to24(selectedtime))+23;
             jQuery('#'+first).show();
             jQuery('#'+second).show();
          }
          
      
          for(var startnew=convertTime12to24(selectedtime); startnew<=parseInt(convertTime12to24(selectedtime))+17; startnew++ )
          {
              jQuery('#'+startnew).show();
              console.log("show converted time here",parseInt(convertTime12to24(selectedtime))+19);
              document.getElementById('_booking_btn').style.display="block";
          }
         
          if(endtime=='' && selectedtime=='11:00 PM' ||  endtime=='' && selectedtime=='10:00 PM' && $('#duration').val()==2)
          {
              endtime = '11:59 PM';
          }
          
          
           if(endtime=='')
          {
              endtime = '11:59 PM';
          }
          jQuery('input[name="endTime"]').val(endtime);
          jQuery('.check-out-render .render').text(endtime);
          jQuery('.input-extra').trigger('change');

            let data = jQuery('form', form).serializeArray();
            data.push({
                name: '_token',
                value: jQuery('meta[name="csrf-token"]').attr('content')
            });
            var  form =jQuery('#form-book-home')
             let loading = $('.hh-loading', form);
            loading.show();
            jQuery('.form-render', form).empty();
            jQuery('.form-message', form).empty();

             
            setTimeout(function(){
            jQuery.post(form.attr('data-real-price'), data, function (respon) {
                if (typeof respon === 'object') {
                    jQuery('.form-render', form).html(respon.html);
                     $('.form-render').show();
                    if (respon.status === 0) {
                       // alert(respon.message);
                    }

                }
                loading.hide();
            }, 'json');
    },300);
          jQuery('.appointmentDates').show();
    }
    
      function isEmpty(str) {
    return (!str || str.length === 0 );
}
    function availabletimes()
    {
        jQuery('#_date_and_time').hide();
         jQuery('.appointmentDates').hide();
        jQuery('#duration').attr('disabled',true);
        var arrayoftime =['00:00 AM','01:00 AM','02:00 AM','03:00 AM','04:00 AM','05:00 AM','06:00 AM','07:00 AM','08:00 AM','09:00 AM','10:00 AM','11:00 AM','12:00 PM','01:00 PM','02:00 PM','03:00 PM','04:00 PM','05:00 PM','06:00 PM','07:00 PM','08:00 PM','09:00 PM','10:00 PM','11:00 PM'];
      
   setTimeout(function(){   if(jQuery('.check-in-render .dropdown-time .item').length>0)
      {
          console.log("available hours check coming here");
          document.getElementById('_date_and_time').style.display="block";
            selectedelements = new Array();
              jQuery('.check-in-render .dropdown-time .item').each(function( index ) {
                  
                  console.log('item value come here',jQuery(this).text());
                      if(arrayoftime.includes(jQuery(this).text()))
                      {
                          var selectedelement = parseInt(arrayoftime.indexOf(jQuery(this).text()))+1
                          jQuery('#'+selectedelement).addClass('available');
                          selectedelements.push(selectedelement);
                        //   document.getElementById('_date_and_time').style.display="block";
                      }else
                      {
                          
                          jQuery('#'+selectedelement).removeClass('available');
                      }
                });
                console.log("array data coming here",selectedelements);
                arrayoftime.forEach(function (item, index) {
      //console.log(item, index);
         if(!selectedelements.includes(index))
         { 
             jQuery('#'+index).removeClass('available');
         }
         
         var weekdayscost ='<?= $weekprice ?>';
         var d = new Date(jQuery('#datechange').text().split('-')[1]+'-'+jQuery('#datechange').text().split('-')[0]+'-'+jQuery('#datechange').text().split('-')[2]);
         console.log("text this ",d.getDay(),jQuery('#datechange').text());
         if(d.getDay()==0 || d.getDay()==6 )
         {
              $('.appointmentSlot').tooltip('disable')
         }
         else
         {
             $('.appointmentSlot').tooltip('enable')
         }
         jQuery('#_date_and_time').show();
         jQuery('#duration').trigger('change');
         jQuery('#duration').attr('disabled',false)
});
          
      }  }, 1100);
        
        
    }
    
    function slectedtimehourmg(event)
    {
        $('#duration').prop("disabled", false); 
        var arrayoftime =['00:00 AM','01:00 AM','02:00 AM','03:00 AM','04:00 AM','05:00 AM','06:00 AM','07:00 AM','08:00 AM','09:00 AM','10:00 AM','11:00 AM','12:00 PM','01:00 PM','02:00 PM','03:00 PM','04:00 PM','05:00 PM','06:00 PM','07:00 PM','08:00 PM','09:00 PM','10:00 PM','11:00 PM'];
           var totalhours = 1;
           arrayoftime.forEach(function (item, index) {
      //console.log(item, index);
              var selectedindex = parseInt(arrayoftime.indexOf(jQuery(event).text()));
                 if(selectedindex==index || selectedindex <index)
                 {
                          
                                 if(jQuery('#'+index).hasClass('available'))
                                 {
                                     
                                     
                                     totalhours=totalhours+1
                                 }
                             
                     
                 }
               
});             
            if(totalhours>0)
            {
               console.log("total hours index check", totalhours);
               
               jQuery("#duration option").each(function(){
                   if(jQuery(this).val()>totalhours)
                   {
                        jQuery(this).attr("disabled", true)
                       
                   }else
                   {
                       jQuery(this).attr("disabled", false)
                   }
    
});
            }
         
        console.log("selected hours",event)
        
    }
    
    document.getElementById('datechange').addEventListener("DOMSubtreeModified", function (event) {
         availabletimes();
         console.log("date change coming here");
  // ...
}, false);
    
window.onload = function(){


    const d = jQuery('#datechange').text().split('-');
         console.log('date selected ',d[2],d[0],d[1]);
          const datestring  = d[2]+'-'+d[1]+'-'+d[0];
         jQuery.ajax({
                      'url':'<?= url("/get-home-availability-time-single")?>',
                      'type':'post',
                       data:{_token:jQuery('input[name="_token"]').val(),home_id:{{$post->post_id}},start:datestring},
                       success:function(response)
                       {
                             
                            console.log("response is coming here ",response);
                            $('.dropdown-time').html(response.html);
                             availabletimes()
                            // $('.dropdown-time').show();
                            //   $('.form-group-date-time').removeClass('d-none');
                           
                            //   $('.appointmentDates').show();

                       }


         })

};   

</script>


