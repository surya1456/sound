<?php
$css = '
        .text-center{
            text-align: center;
        }
          .CToWUd {
    width: 51%!important;
}
        .hh-email-wrapper{
            width: 95%;
            max-width: 650px;
            margin: 20px auto;
            border: 1px solid #EEE;
            padding: 20px;
        }
        .hh-email-wrapper .header-email table{
            width: 100%;
            border: none;
            table-layout: fixed;
        }
        .hh-email-wrapper .header-email .logo{
           width: 60px;
           height: auto;
        }
        .hh-email-wrapper .header-email .description{
            font-size: 18px;
            text-transform: uppercase;
            letter-spacing: 1px;
            margin-bottom: 0;
            margin-top: 0;
        }
        .content-email{
            padding-top: 20px;
            padding-bottom: 30px;
        }

        .booking-detail{
            margin-top: 30px;
            padding: 10px 15px;
            border: 1px solid #EEE;
        }
        .booking-detail .item{
            padding-top: 15px;
            padding-bottom: 15px;
            border-bottom: 1px solid #EEE;
            display: flex;
        }
        .booking-detail .item .title{
            display: inline-block;
            font-size: 15px;
            width: 35%;
        }
        .booking-detail .item .info{
            display: inline-block;
            font-size: 15px;
            font-weight: bold
        }
        .booking-detail .client-info .info{
            font-weight: normal;
        }
        .booking-detail .client-info .info p{
            margin-top: 0;
        }
        .button-primary{
            display: inline-block;
            padding: 10px 20px;
            border: 1px solid #f8546d;
            background: #f8546d;
            color: #FFF;
            text-align: center;
        }
        .confirmation-button{
            margin-top: 30px;
            text-transform: uppercase;
            text-decoration: none;
        }

        .footer-email{
            border-top: 1px solid #EEE;
            padding: 30px 0;
        }
    ';

$service_data = get_booking_data($booking->ID, 'serviceObject');
start_get_view();
?>
<div class="hh-email-wrapper">
    <div class="header-email">
        <table>
            <tr>
                <td style="text-align: left">
                    <?php
                    $logo = get_option('email_logo');
                    $logo_url = get_attachment_url($logo);
                    ?>
                    <a href="{{ url('/') }}" target="_blank">
                        <img src="{{ $logo_url }}" alt="{{ get_option('site_description') }}" class="logo" style="width:50%!important">
                    </a>
                </td>
                <td style="text-align: right">
                    <h4 class="description">{{ get_option('site_name') }}</h4>
                </td>
            </tr>
        </table>
    </div>
   
    <div class="content-email">
        <p>{{__('Hello')}} <strong>{{$booking->first_name}} {{$booking->last_name }}</strong>,</p>
        <h1>{{__('Your Soundbox Studios Booking Confirmation')}}</h1>
        <p><strong>{{__('Thanks for booking a studio with us!')}}</strong></p>
        <p>{{__('All your booking details can be found below and in My Bookings section of your Soundbox account. If anything is broken or missing when you arrive please contact us immediately on Live Chat or via hello@soundboxstudios.com.au')}}</p>
        <p><strong>{{__('When friends sign up with your referral code you will both get $15. For them on their first booking, and for you on your next one!')}}</strong></p>
        <p><strong> <a href="<?= url('/') ?>/auth/sign-up?ref={{picodecheck($booking->buyer,'refferal_key')}}">Click HERE TO "REFER A FRIEND"</a></strong></p>
        
         <?php // $roomdetail =  \DB::select('post_title')->from('home')->where('id','=',$booking->service_id)->first(); ?>
  
        <?php 
     

       
         $roomtype = \DB::table('term_relation')->select('term_title')->join('term','term_relation.term_id', '=', 'term.term_id')->where('term_relation.service_id','=',$booking->service_id)->where('term_relation.post_type','=','home')->where('term.taxonomy_id','=',1)->first();
        ?>
              <?php
                $user_data = get_booking_data($booking->ID, 'user_data');
                ?>
                 <?php
                    $paymentID = $booking->payment_type;
                    $paymentObject = get_payments($paymentID);
                    ?>
        <h2>{{__('Booking details')}}</h2>
        <p>{{__('Booking ID: ')}}   {{  $booking->booking_id }}</p>  
        <p>{{__('Amount: ')}}   {{  convert_price($booking->total) }}</p>
        <p>{{__('Room Number: ')}}   {{ get_translate($service_data->post_title)}}</p>
        <p>{{__('Room Type: ')}}   {{$roomtype->term_title}}</p>
        <p>{{__('Location: ')}}{{ $user_data['address'] }} | {{ $user_data['city'] }} | {{ $user_data['postCode'] }}</p>
        <p>{{__('Date: ')}} {{ date(hh_date_format(), $booking->created_date) }}</p>
         <p>{{__('Time: ')}} {{ date('h:i A', $booking->start_date) }}</p>
        <p>{{__('Main door: ')}} {{picodecheck(2559,'main_door')}}</p>  
        <p>{{__('Room code: ')}} {{picodecheck($booking->buyer,'lockpin_'. $booking->booking_id)}} </p>
        <br>  
        <p>{{__('Please read out terms and conditions here.')}}</p>
        <p>{{__('For band studios here is your ')}}<a href="{{ url('https://soundbox-studios.zammad.com/help/en-gb/3-booking-a-studio/3-rehearsal-booking-information') }}">guide</a>.</p>
        <p>{{__('For DJ studios here is your ')}}<a href="{{ url('https://soundbox-studios.zammad.com/help/en-gb/3-booking-a-studio/5-dj-studio-booking-information') }}">guide</a>.</p>
        
        <!--div class="booking-detail">
            <div class="item">
                <span class="title">{{__('Booking ID:')}}</span>
                <span class="info">{{ $booking->booking_id }}</span>
            </div>
         
            <div class="item">
                <span class="title">{{__('Amount:')}}</span>
                <span class="info">{{ convert_price($booking->total) }}</span>
            </div>
            <div class="item">
                <span class="title">{{__('Payment Method:')}}</span>
                <span class="info">
                    
                    {{ $paymentObject::getName() }}
                </span>
            </div>
            <div class="item">
                <span class="title">{{__('Created At:')}}</span>
                <span class="info"></span>
            </div>
            <div class="item client-info">
                <span class="title">{{__('Your information:')}}</span>
              
                <span class="info">
                    <p>{{ $user_data['firstName'] }} {{ $user_data['lastName'] }}</p>
                    <p>{{ $user_data['email'] }}</p>
                    <p>{{ $user_data['phone'] }}</p>
                    <p></p>
                    @if($booking->note)
                        <p>{{__('Note:')}} {{ $booking->note }}</p>
                    @endif
                </span>
            </div>
        </div-->
        <?php
        $encrypt = create_confirmation_code($booking);
        ?>
        <div class="text-center">
            <a class="button-primary confirmation-button"
               href="{{ dashboard_url('booking-confirmation?token='.$booking->token_code.'&code='. $encrypt) }}">{{__('Confirm')}}</a>
        </div>
    </div>
    <div class="footer-email">
        &copy; {{ date('Y') }} - {{ get_option('site_name') }} | {{ get_option('site_description') }}
    </div>
</div>
<?php
$content = end_get_view();
$render = new Emogrifier();
$render->setHtml($content);
$render->setCss($css);
$mergedHtml = $render->emogrify();
$mergedHtml = str_replace('<!DOCTYPE html>', '', $mergedHtml);
unset($render);
?>
{!! balanceTags($mergedHtml) !!}
