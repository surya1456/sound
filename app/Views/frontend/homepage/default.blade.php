@include('frontend.components.header')
<?php
enqueue_style('home-slider');
enqueue_script('home-slider');

enqueue_style('mapbox-gl-css');
enqueue_style('mapbox-gl-geocoder-css');
enqueue_script('mapbox-gl-js');
enqueue_script('mapbox-gl-geocoder-js');

enqueue_style('daterangepicker-css');
enqueue_script('daterangepicker-js');
enqueue_script('daterangepicker-lang-js');

enqueue_style('iconrange-slider');
enqueue_script('iconrange-slider');

enqueue_script('owl-carousel');
enqueue_style('owl-carousel');
enqueue_style('owl-carousel-theme');

$tab_services = list_tabs_service();

?>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=B612:ital,wght@0,400;1,400;1,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=B612:wght@700&display=swap" rel="stylesheet">

<style>
.mySlides {display:none;}
</style>
<div class="home-page pb-5">
    @if(!empty($tab_services))
        <div class="hh-search-form-wrapper">
           <div class="w3-content w3-display-container">
                <?php
                    $sliders = get_option('home_slider');
                    $sliders = explode(',', $sliders);
                    ?>
  @if(!empty($sliders) && is_array($sliders))
                        @foreach($sliders as $id)
 <?php
                            $url = get_attachment_url($id);
                            ?>

<img class="mySlides" src="{{ $url }}" style="width:100%">
               @endforeach
                    @endif

  

  <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
  <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
</div>
        
        </div>
        <script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>

<style>
    .awe-booking .hh-list-destinations .hh-destination-item .thumbnail .detail {
    position: absolute;
    width: 100%;
    top: 50%;
    left: 0;
    -webkit-transform: translateY(-50%);
    transform: translateY(-50%);
    z-index: 9;
}
.inframe {
    margin-bottom: 50px;
    border-radius: 10px;
}
</style>

    @endif
    <div class="container">
        @if(!empty($tab_services))
            <h2 class="h3 mt-5 mb-3">{{__('Explore AweBooking')}}</h2>
            <div class="list-of-featured-services">
                <div class="row">
                    @foreach($tab_services as $service)
                        <?php
                        if (isset($service['only_search_form'])) {
                            continue;
                        }
                        $featured_image = get_option($service['id'] . '_featured_image');
                        $image_url = get_attachment_url($featured_image, 'full');
                        $func = 'get_' . $service['id'] . '_search_page';
                        $search_url = function_exists($func) ? $func() : url('/');
                        ?>
                        <div class="col-4">
                            <div class="item">
                                <a href="{{$search_url}}" target="_blank">
                                    <div class="row">
                                        <div class="col-12 col-md-5">
                                            <div class="thumbnail">
                                                <div class="thumbnail-outer">
                                                    <div class="thumbnail-inner">
                                                        <img src="{{ $image_url }}"
                                                             alt="{{ __($service['label']) }}"
                                                             class="img-fluid">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-7 d-md-flex align-items-center">
                                            <h2 class="title">{{ __($service['label']) }}</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    <!-- Home in New York -->
        @if(is_enable_service('home'))
            <?php
            $list_services = \App\Controllers\Services\HomeController::get_inst()->listOfHomes([
                'number' => 4,
               
            ]);
            ?>
            @if(count($list_services['results']))
                <h2 class="h3 mt-4">{{__('Homes in New York')}}</h2>
                <p>{{__('Browse beautiful places to stay with all the comforts of home, plus more')}}</p>
                <div class="hh-list-of-services">
                    <div class="row">
                        @foreach($list_services['results'] as $item)
                            <div class="col-12 col-md-6 col-lg-3">
                                @include('frontend.home.loop.grid', ['item' => $item])
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        @endif
    <!-- Destination -->
        <?php
        $locations = get_option('top_destination');
        ?>
        @if(!empty($locations))
            <h2 class="h3 mt-4">{{__('Top destinations')}}</h2>
           
            <div class="hh-list-destinations">
                <?php
                $responsive = [
                    0 => [
                        'items' => 1
                    ],
                    768 => [
                        'items' => 2
                    ],
                    992 => [
                        'items' => 3
                    ],
                ];
                ?>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3566.6639108495224!2d152.9582614150938!3d-26.62721758777072!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b9378fc368449f9%3A0xf2ba6df1c02cc573!2s36%20Howard%20St%2C%20Nambour%20QLD%204560%2C%20Australia!5e0!3m2!1sen!2sin!4v1629348058978!5m2!1sen!2sin" width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                
                <!--<div class="hh-carousel carousel-padding nav-style2"-->
                <!--     data-responsive="{{ base64_encode(json_encode($responsive)) }}" data-margin="15" data-loop="0">-->
                   
                        
                <!--             <a href="<?= url('/home-search-result') ?>"> <div class="thumbnail has-matchHeight">-->
                <!--                <div class="thumbnail-outer">-->
                <!--                                <div class="thumbnail-inner">-->
                <!--                                    <img src="public/images/New Project (57).jpg" style="width:100%;border-radius: 20px;">-->
                <!--                                </div>-->
                <!--                            </div>-->
                                
                <!--              <div class="detailss">-->
                <!--                                <h2 class="text-center des-paterm-1">Nambour</h2>-->
                <!--                                                                            </div>-->
                <!--            </div>-->
                <!--      </a>-->
                  
                <!--    <div class="owl-nav">-->
                <!--        <a href="javascript:void(0)"-->
                <!--           class="prev"><i class="ti-angle-left"></i></a>-->
                <!--        <a href="javascript:void(0)"-->
                <!--           class="next"><i class="ti-angle-right"></i></a>-->
                <!--    </div>-->
                <!--</div>-->
            </div>
        @endif
    <!-- Experience Types -->
        <?php
        $experience_types = get_terms('experience-type', true);
        ?>
        @if(count($experience_types) > 0)
            <h2 class="h3 mt-4">{{__('Find a Experience type')}}</h2>
            <div class="hh-list-terms mt-3">
                @if(count($experience_types))
                    <?php
                    $responsive = [
                        0 => [
                            'items' => 1
                        ],
                        768 => [
                            'items' => 2
                        ],
                        992 => [
                            'items' => 3
                        ],
                        1200 => [
                            'items' => 4
                        ]
                    ];
                    ?>
                    <div class="hh-carousel carousel-padding nav-style2"
                         data-responsive="{{ base64_encode(json_encode($responsive)) }}" data-margin="15" data-loop="0">
                        <div class="owl-carousel">
                            @foreach($experience_types as $item)
                                <?php
                                $url = get_attachment_url($item->term_image, [350, 300]);
                                ?>
                                <div class="item">
                                    <div class="hh-term-item">
                                        <a href="{{ get_experience_search_page('?experience-type=' . $item->term_id) }}">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="thumbnail has-matchHeight">
                                                        <div class="thumbnail-outer">
                                                            <div class="thumbnail-inner">
                                                                <img src="{{ $url }}"
                                                                     alt="{{ get_attachment_alt($item->term_image ) }}"
                                                                     class="img-fluid">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6 d-flex align-items-center">
                                                    <div class="clearfix">
                                                        <h4>{{ get_translate($item->term_title) }}</h4>
                                                        <?php
                                                        $home_count = count_experience_in_experience_type($item->term_id);
                                                        ?>
                                                        <p class="text-muted">{{ sprintf(__('%s Experiences'), $home_count) }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="owl-nav">
                            <a href="javascript:void(0)"
                               class="prev"><i class="ti-angle-left"></i></a>
                            <a href="javascript:void(0)"
                               class="next"><i class="ti-angle-right"></i></a>
                        </div>
                    </div>
                @endif
            </div>
        @endif
    <!-- Experience in Ha Noi -->
        @if(is_enable_service('experience'))
            <?php
            $list_services = \App\Controllers\Services\ExperienceController::get_inst()->listOfExperiences([
                'number' => 4,
                'location' => [
                    'lat' => '21',
                    'lng' => '105.75',
                    'radius' => 50
                ],
                'order' => 'rand'
            ]);
            ?>
            @if(count($list_services['results']))
                <h2 class="h3 mt-4">{{__('Popular experiences in Ha Noi')}}</h2>
                <p>{{__('Book activities led by local hosts on your next trip')}}</p>
                <div class="hh-list-of-services mt-p">
                    <div class="row">
                        @foreach($list_services['results'] as $item)
                            <?php $item = setup_post_data($item, 'experience'); ?>
                            <div class="col-12 col-md-6">
                                @include('frontend.experience.loop.list', ['item' => $item])
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        @endif
    </div>

    <!-- Call to action -->
    <?php
    $page_id = get_option('call_to_action_page');
    $cta_background_id = get_option('call_to_action_background', '');
    ?>
    @if(!empty($page_id))
        <?php
        $link = get_permalink_by_id($page_id, 'page');
        $cta_background_url = '';
        $cta_background_url = get_attachment_url($cta_background_id, 'full');
        ?>
        <!--<div class="container mt-4 mb-4">-->
        <!--    <div class="call-to-action pl-4 pr-4 has-background-image" data-src="{{ $cta_background_url }}"-->
        <!--         style="background-image: url('{{$cta_background_url}}')">-->
        <!--        <div class="row">-->
        <!--            <div class="col-lg-12">-->
        <!--                <h5 class="main-text">{{__('The most exciting trip this summer')}}</h5>-->
        <!--                <p class="sub-text">{{__('Enjoy moments at the beach Maldives with friends')}}</p>-->
        <!--            </div>-->
                    <!--<div class="col-lg-4">-->
                    <!--    <a href="{{ $link }}" class="btn btn-primary right">{{__('Watch now')}}</a>-->
                    <!--</div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
    @endif
    <div class="container">
        <!--Featured Car -->
        @if(is_enable_service('car'))
            <?php
            $list_services = \App\Controllers\Services\CarController::get_inst()->listOfCars([
                'number' => 8,
                'is_featured' => 'on'
            ]);
            ?>
            @if(count($list_services['results']))
                <h2 class="h3 mt-4 ">{{__('Featured Cars')}}</h2>
                <p>{{__('Book incredible things to do around the world.')}}</p>
                <div class="hh-list-of-services">
                    <?php
                    $responsive = [
                        0 => [
                            'items' => 1
                        ],
                        768 => [
                            'items' => 2
                        ],
                        992 => [
                            'items' => 3
                        ],
                        1200 => [
                            'items' => 4
                        ],
                    ];
                    ?>
                    <div class="hh-carousel carousel-padding nav-style2"
                         data-responsive="{{ base64_encode(json_encode($responsive)) }}" data-margin="15" data-loop="0">
                        <div class="owl-carousel">
                            @foreach($list_services['results'] as $item)
                                <div class="item">
                                    @include('frontend.car.loop.grid', [
                                                    'item' => $item
                                                ])
                                </div>
                            @endforeach
                        </div>
                        <div class="owl-nav">
                            <a href="javascript:void(0)"
                               class="prev"><i class="ti-angle-left"></i></a>
                            <a href="javascript:void(0)"
                               class="next"><i class="ti-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            @endif
        @endif
    </div>
    <!-- Testimonial -->
    <?php
    $testimonials = get_option('testimonial', []);
    $responsive = [
        0 => [
            'items' => 1
        ],
        768 => [
            'items' => 2
        ],
        992 => [
            'items' => 2
        ],
        1200 => [
            'items' => 3
        ],
    ];

    $testimonial_bgr = get_option('testimonial_background', '#dd556a');
    ?>
    @if(count($testimonials) and false)
        <div class="section section-background pt-5 pb-5 mt-4" style="background-color: {{$testimonial_bgr}};">
            <div class="container">
                <h2 class="h3 mt-0 c-white">{{__('Say about Us')}}</h2>
                <p class="c-white">{{__('Browse beautiful places to stay with all the comforts of home, plus more')}}</p>
                <div class="hh-testimonials">
                    <div class="hh-carousel carousel-padding nav-style2"
                         data-responsive="{{ base64_encode(json_encode($responsive)) }}" data-margin="30" data-loop="0">
                        <div class="owl-carousel">
                            @foreach($testimonials as $testimonial)
                                <div class="item">
                                    <div class="testimonial-item">
                                        <div class="testimonial-inner">
                                            <div class="author-avatar">
                                                <img
                                                    src="{{ get_attachment_url($testimonial['author_avatar'], [80, 80]) }}"
                                                    alt="{{get_translate( $testimonial['author_name']) }}"
                                                    class="img-fluid">
                                                <i class="mdi mdi-format-quote-open hh-icon"></i>
                                            </div>
                                            <div class="author-rate">
                                                @include('frontend.components.star', ['rate' => (int) $testimonial['author_rate']])
                                            </div>
                                            <div class="author-comment">
                                                {{ get_translate($testimonial['author_comment']) }}
                                            </div>
                                            <h2 class="author-name">
                                                {{ get_translate($testimonial['author_name']) }}
                                            </h2>
                                            @if($testimonial['date'])
                                                <div class="author-date">{{sprintf(__('on %s'), date(hh_date_format(), strtotime($testimonial['date'])))}}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="owl-nav">
                            <a href="javascript:void(0)"
                               class="prev"><i class="ti-angle-left"></i></a>
                            <a href="javascript:void(0)"
                               class="next"><i class="ti-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endif
<!-- List of Blog -->
    <div class="container">
        <?php
        $list_services = \App\Controllers\PostController::get_inst()->listOfPosts([
            'number' => 2
        ]);
        $responsive = [
            0 => [
                'items' => 1
            ]
        ];
        ?>
        @if(count($list_services['results']))
            <h2 class="h3 mt-4 mb-3">{{__('The latest from Blog')}}</h2>
            <div class="hh-list-of-blog">
                <div class="row">
                    @foreach($list_services['results'] as $item)
                        <div class="col-12 col-md-6">
                            <div class="hh-blog-item style-2">
                                <a href="{{ get_the_permalink($item->post_id, $item->post_slug, 'post') }}">
                                    <div class="thumbnail">
                                        <div class="thumbnail-outer">
                                            <div class="thumbnail-inner">
                                                <img src="{{ get_attachment_url($item->thumbnail_id, 'full') }}"
                                                     alt="{{ get_attachment_alt($item->thumbnail_id ) }}"
                                                     class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="category">{{__('Action')}}
                                    <div class="date">{{ date(hh_date_format(), $item->created_at) }}</div>
                                </div>
                                <h2 class="title"><a
                                        href="{{ get_the_permalink($item->post_id, $item->post_slug, 'post') }}">{{ get_translate($item->post_title) }}</a>
                                </h2>
                                <div
                                    class="description">{!! balanceTags(short_content(get_translate($item->post_content), 55)) !!}</div>
                                <div class="w-100 mt-2"></div>
                                <div class="d-flex justify-content-between">
                                    <?php
                                    $url = get_the_permalink($item->post_id, $item->post_slug, 'post');
                                    $img = get_attachment_url($item->thumbnail_id);
                                    $desc = get_translate($item->post_title);

                                    $share = [
                                        'facebook' => [
                                            'url' => $url
                                        ],
                                        'twitter' => [
                                            'url' => $url
                                        ],
                                        'pinterest' => [
                                            'url' => $url,
                                            'img' => $img,
                                            'description' => $desc
                                        ]
                                    ];
                                    ?>
                                    @include('frontend.components.share', ['share' => $share])
                                    <a href="{{ get_the_permalink($item->post_id, $item->post_slug, 'post') }}"
                                       class="read-more">{{__('Keep Reading')}} {!! balanceTags(get_icon('002_right_arrow', '#F8546D', '12px', '')) !!}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</div>
@include('frontend.components.footer')


